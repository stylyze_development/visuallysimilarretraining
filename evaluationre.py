import csv
import numpy as np
import pandas as pd
import pickle
import re

# import vsnm.evaldatare as data
import vsnm.evaldataretarget as data
import vsnm.evalre as model
import vsnm.submissionre as submission
import vsnm.configurationre as configuration
import vsnm.tojsonre as tojson

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():

    args = configuration.parse_arguments()

    model_input_path = data.load_data(args["database_path"],
                                      args["output_table_path"],
                                      args["column_names"])

    # model_input_path = "./output_tables/deep_learning_df"

    model_output_path, model_output_path_2 = model.embedding(model_input_path,
                                                             args["n_H0"],
                                                             args["n_W0"],
                                                             args["n_C0"],
                                                             args[
                                                                 "batch_size"],
                                                             args[
                                                                 "training_ratio"],
                                                             args[
                                                                 "embedding_size"],
                                                             args[
                                                                 "search_size"],
                                                             args[
                                                                 "search_size_2"],
                                                             args[
                                                                 "image_normalization"],
                                                             args[
                                                                 "same_image_epsilon"],
                                                             args[
                                                                 "model_weight_path"],
                                                             args[
                                                                 "output_table_path_2"],
                                                             args[
                                                                 "model_name"],
                                                             args[
                                                                 "output_layer"],
                                                             args["output_table_path_9"],
                                                             args["keepratio"])

    # model_input_path = "./output_tables/deep_learning_df"
    # model_output_path = "./output_tables/deep_learning_dic"
    # model_output_path_2 = "./output_tables/deep_learning_score_dic"

    submission.output_lsbs(
        model_input_path, model_output_path, model_output_path_2, args["output_table_path_3"])

    tojson.output_json(args["output_table_path_3"], args["output_table_path_12"])

if __name__ == '__main__':
    main()
