import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf


class DataGenerator(object):

    def __init__(self, dim_x=400, dim_y=400, dim_z=3, batch_size=64,
                 image_normalization=250., n_y=1, shuffle=False):

        self.dim_x = dim_x
        self.dim_y = dim_y
        self.dim_z = dim_z
        self.batch_size = batch_size
        self.image_normalizatio = image_normalization
        self.n_y = n_y
        self.shuffle = shuffle

    def generate(self, labels, list_IDs, image_path):

        while 1:

            # Generate order of exploration of dataset
            indexes = self.__get_exploration_order(list_IDs)

            # Generate batches
            imax = int(len(indexes) / self.batch_size)
            for i in range(imax):

                # Find list of ids
                list_IDs_temp = [list_IDs[k] for k in indexes[
                    i * self.batch_size:(i + 1) * self.batch_size]]

                # Generate data
                X, y, bad_img = self.__data_generation(
                    labels, list_IDs_temp, image_path)

                yield X, y, bad_img

    def __get_exploration_order(self, list_IDs):

        # Find exploration order
        indexes = np.arange(len(list_IDs))
        if self.shuffle == True:
            np.random.shuffle(indexes)

        return indexes

    def __data_generation(self, labels, list_IDs_temp, image_path):

        # X : (n_samples, v_size, v_size, v_size, n_channels)
        # Initialization
        X = np.empty(
            (self.batch_size, self.dim_x, self.dim_y, self.dim_z))
        y = np.empty((self.batch_size, self.n_y), dtype=int)

        vgg_mean = np.expand_dims([123.68,116.779,103.939], axis=0)
        vgg_mean = np.expand_dims(vgg_mean, axis=0)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):

            image_path_int = image_path[int(ID)]
            img_0 = cv2.imread(image_path_int)
            if img_0 is not None:
                img_0 = cv2.resize(img_0, (self.dim_x, self.dim_y))
                img_0 = img_0[...,::-1].astype(np.float32)
                # b,g,r = cv2.split(img_0)
                # img_0 = cv2.merge([r,g,b])
                img_0 = img_0 - vgg_mean
                X[i, :, :, :] = np.array(img_0)
                # img_0 = cv2.imread(image_path_int)
                # img_0 = cv2.resize(img_0, (self.dim_x, self.dim_y))
                # X[i, :, :, :] = np.asarray(img_0) / self.image_normalization
                bad_img = 0
            else:
                bad_img = 1
                X[i, :, :, :] = np.zeros((self.dim_x, self.dim_y, 3))
                logging.basicConfig(level=logging.INFO)
                logging.info("***NO JPG FOR ITEM" + " " + ID + " " + "(INDEX)")
                # print("NO JPG FOR ITEM" + " " + ID + "(INDEX)")

            # image_path_int = image_path[int(ID)]
            # try:
            #     img_0 = cv2.imread(image_path_int)
            #     img_0 = cv2.resize(img_0, (self.dim_x, self.dim_y))
            #     img_0 = img_0[...,::-1].astype(np.float32)
            #     # b,g,r = cv2.split(img_0)
            #     # img_0 = cv2.merge([r,g,b])
            #     img_0 = img_0 - vgg_mean
            #     X[i, :, :, :] = np.array(img_0)
            #     # img_0 = cv2.imread(image_path_int)
            #     # img_0 = cv2.resize(img_0, (self.dim_x, self.dim_y))
            #     # X[i, :, :, :] = np.asarray(img_0) / self.image_normalization
            #     bad_img = 0
            # except cv2.error as e:
            #     bad_img = 1
            #     X[i, :, :, :] = np.zeros((self.dim_x, self.dim_y, 3))

            y[i] = labels[ID]

        return X, y, bad_img
