import csv
import cv2
from datetime import datetime
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

# Use embeddings to rank similar products


def top_k_products(similarity, mapper, product_idx,
                   search_size=21, search_size_2=11,
                   same_image_epsilon=0.00000001):

        # return [mapper[x] for x in
        # np.argsort(similarity[product_idx,:])[:-k-1:-1]]
    list_temp = [mapper[x] for x in np.argsort(
        similarity[product_idx, :])[:-search_size - 1:-1]]
    list_values = [similarity[product_idx, x] for x in list_temp]

    for i in range(len(list_values)):
        for j in range(i + 1, len(list_values)):
            if i != len(list_values) - 1:
                if np.abs(list_values[j] - list_values[i]) < same_image_epsilon:
                    list_values[j] = -1

    return [list_temp[x] for x in np.argsort(list_values)[:-search_size_2 - 1:-1]]


def top_k_products_parser(idx, search_size_2, products_temp, same_image_epsilon, sim_idx, index):

    products = products_temp[:search_size_2]
    products[0] = idx
    if idx in products_temp:

        products_temp.remove(idx)
        products[1:] = products_temp[0:search_size_2 - 1]

    else:

        products[1:] = products_temp[0:search_size_2 - 1]

    for i in products[1:]:
        if 1 - sim_idx[i] < same_image_epsilon:
            products.remove(i)

    products_2 = [-1] * search_size_2
    sim_score = [-1] * search_size_2
    j = 0
    for ii in products:
        products_2[j] = int(index[ii])
        sim_score[j] = sim_idx[ii]
        j += 1

    return products_2, sim_score
