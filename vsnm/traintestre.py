import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re
import copy
from datetime import datetime

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path):
    """

    """

    startTime = datetime.now()
    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)

    print("**************************************")

    df = df.sample(frac=1).reset_index(drop=True)

    # df_train = df.loc[:48000]
    df_train = df 
    df_train = df_train.reset_index(drop=True)

    print("len(df_train)", len(df_train))
    print(df_train.head(5))

    df_test = df.loc[48000:]
    df_test = df_test.reset_index(drop=True)

    # print(len(df_test))
    # print(df_test.head(10))

    df_train.to_csv(cwd + "/data_tables/df_train.csv", index=False)
    path_train = "/data_tables/df_train.csv"

    df_test.to_csv(cwd + "/data_tables/df_test.csv", index=False)
    path_test = "/data_tables/df_test.csv"

    return path_train, path_test
