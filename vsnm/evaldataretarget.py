import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)
    # df = pd.read_csv(cwd + "/data_tables/df_train.csv")

    print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES"
                            })

    # Pull the columns needed from the big table
    df = df[["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "ARCHIVED"]]
    df = df.fillna("Missing")
    df = df[df["ARCHIVED"] != 1]

    # The following functions are used to parse the type and style attributes
    def parser(str_input):
        if str_input != "Missing":
            # a = str_input.replace(" ", "")
            a = re.split('[;]', str_input)
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_2(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            # a = a[0]
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_0(str_input):
        return str(str_input)        

    # Apply the parsers to the types and styles
    df["TYPES"] = df.apply(lambda row: parser(row["TYPES"]), axis=1)
    df["VARIANT_ID"] = df.apply(lambda row: parser_0(row["VARIANT_ID"]), axis=1)
    # df["STYLES"] = df.apply(lambda row: parser_2(row["STYLES"]), axis=1)
    # df["CATEGORIES"] = df.apply(lambda row: parser_2(row["CATEGORIES"]), axis=1)

    df_big = df
    # df_big["VARIANT_ID"] = df_big["VARIANT_ID"].astype('int64')

    # Drop the items with missing and/or uncategorized type and/or style
    # df_big = df_big[df_big["STYLES"] != "Missing"]
    df_big = df_big[df_big["TYPES"] != "Missing"]
    # df_big = df_big[df_big["CATEGORIES"] != "Missing"]
    # df_big = df_big[df_big["STYLES"] != "Uncategorized"]
    # df_big = df_big[df_big["TYPES"] != "uncategorized"]

    # print(df_big["TYPES"])

    # df_big = df_big[(df_big["TYPES"] == "accent chair") | (df_big["TYPES"] == "office chair") | (df_big["TYPES"] == "dining chair")]

    # df_big = df_big[(df_big["TYPES"] == "day bed") | (df_big["TYPES"] == "bed") | (df_big["TYPES"] == "guest bed") | (df_big["TYPES"] == "master bed")]

    # df_big = df_big[(df_big["TYPES"] == "coffee table") | (df_big["TYPES"] == "end table") | (df_big["TYPES"] == "dining table") | (df_big["TYPES"] == "bistro table")]

    df_big = df_big[(df_big["TYPES"] == "area rug") | (df_big["TYPES"] == "kitchen rug") | (df_big["TYPES"] == "outdoor area rug") |
                    (df_big["TYPES"] == "outdoor rugs") | (df_big["TYPES"] == "round rug") | (df_big["TYPES"] == "outdoor runner") |
                    (df_big["TYPES"] == "runner") | (df_big["TYPES"] == "accent rug")]

    # df_big = df_big[(df_big["TYPES"] == "decorative pillow")]

    # df_big = df_big[(df_big["TYPES"] == "wall art") | (df_big["TYPES"] == "wall decor") | (df_big["TYPES"] == "bathroom wall art") |
    #                 (df_big["TYPES"] == "kitchen wall art") | (df_big["TYPES"] == "sports wall art") | (df_big["TYPES"] == "wall art items")]

    # df_big = df_big[(df_big["TYPES"] == "bedroom set") | (df_big["TYPES"] == "bed") | (df_big["TYPES"] == "bunk bed") |
    #                 (df_big["TYPES"] == "guest bed") | (df_big["TYPES"] == "daybed")]

    # df_big = df_big[(df_big["TYPES"] == "headboard")]   
     
    print("NUMBER OF LSBS:" + str(len(df_big)))

    df_big = df_big.reset_index(drop=True)

    cwd = os.getcwd()

    model_input_path = cwd + output_table_path
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_big, my_file, protocol=2)

    return model_input_path
