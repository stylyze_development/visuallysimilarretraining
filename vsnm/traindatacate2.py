import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re
import random
import copy
from datetime import datetime


def load_data(output_table_path,
              output_table_path_4,
              output_table_path_5,
              output_table_path_6,
              column_names,
              num_items,
              train_or_test):

    option = "CURATOR_IMAGE_PATH"
    # option = "IMAGE_URL"

    # num_items = (search_size_2 - 1) * 2 + 1

    # Load NM input csv as a reference
    cwd = os.getcwd()

    with open(cwd + output_table_path, "rb") as my_file2:
    # with open(cwd + "/output_tables/deep_learning_df_aug", "rb") as my_file2:    
        # deep_learning_df = pickle.load(my_file2,encoding='latin1')
        deep_learning_df = pickle.load(my_file2)

    deep_learning_df_original = deep_learning_df.copy(deep=True)
    # print(deep_learning_df)

    # Rename the column names
    # deep_learning_df = deep_learning_df.rename(columns={column_names[0]: "VARIANT_ID",
    #                                                     column_names[1]: "TYPES",
    #                                                     column_names[2]: "STYLES",
    #                                                     column_names[3]: "CURATOR_IMAGE_PATH",
    #                                                     column_names[4]: "IMAGE_URL",
    #                                                     column_names[5]: "CATEGORIES"
    #                                                     })

    # Load the table with positive examples
    deep_learning_index = pd.read_csv(cwd + output_table_path_5)

    # print(deep_learning_index.head(10))

    # Count the number of items including the seed + similar items
    counter_item = int((num_items - 1) / 2)
    # for j in range(len(deep_learning_index.loc[0]) - 1):
    #     counter_item += 1
    print("******************************************************************")
    print("len(deep_learning_index) = ", len(deep_learning_index))
    print(deep_learning_index.head(5))

    # Convert the table with positive examples to another table with two columns;
    # the first column is the anchor item and the second column is the
    # positive item
    jj = 0
    df_triplet_temp = np.zeros(
        (len(deep_learning_index) * counter_item, 2)).astype(int)
    for i in range(len(deep_learning_index)):
        for j in range(counter_item):
            df_triplet_temp[jj, 0] = int(deep_learning_index.loc[i][1])
            df_triplet_temp[jj, 1] = int(deep_learning_index.loc[i][1 + j + 1])
            jj += 1

    df_triplet_temp_2 = copy.deepcopy(df_triplet_temp)         

    # print(df_triplet_temp[70, 1])
    print(df_triplet_temp[:5])
    print("len(df_triplet_temp) = ", len(df_triplet_temp))

    VARIANT_ID = []
    ANCHOR = []
    ANCHOR_CURATOR_IMAGE_PATH = []
    POSITIVE = []
    POSITIVE_CURATOR_IMAGE_PATH = []
    positive_sub_all = []
    no_vs_items = int(len(df_triplet_temp) / counter_item)
    for i in range(no_vs_items):
        positive_sub_counter = []
        for sub_i in range(counter_item):
            sub_counter = i * counter_item + sub_i
            VARIANT_ID.append(deep_learning_df["VARIANT_ID"][
                              df_triplet_temp[sub_counter, 0]])
            ANCHOR.append(int(df_triplet_temp[sub_counter, 0]))
            ANCHOR_CURATOR_IMAGE_PATH.append(
                deep_learning_df[option][df_triplet_temp[sub_counter, 0]])

            if df_triplet_temp[sub_counter, 1] != -1:
                POSITIVE.append(int(df_triplet_temp[sub_counter, 1]))
                POSITIVE_CURATOR_IMAGE_PATH.append(
                    deep_learning_df[option][df_triplet_temp[sub_counter, 1]])
            else:
                POSITIVE.append(int(df_triplet_temp[sub_counter, 0]))
                POSITIVE_CURATOR_IMAGE_PATH.append(
                    deep_learning_df[option][df_triplet_temp[sub_counter, 0]])

            if df_triplet_temp[sub_counter, 1] != -1:
                positive_sub_counter.append(sub_counter)
        positive_sub_all.append(positive_sub_counter)

        for sub_i in range(counter_item):
            sub_counter = i * counter_item + sub_i
            if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter != []:
                 
                random_positive = random.choice(positive_sub_counter)
                POSITIVE[sub_counter] = (
                    int(df_triplet_temp[random_positive, 1]))
                POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = (
                    deep_learning_df[option][df_triplet_temp[random_positive, 1]])

            if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter == []:
                # POSITIVE[sub_counter] = ANCHOR[sub_counter]
                # POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = deep_learning_df["CURATOR_IMAGE_PATH"][ANCHOR[sub_counter]]
                POSITIVE[sub_counter] = 155025
                # POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = deep_learning_df[option][55025]
                POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = " "    

            # if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter == []:
            #     POSITIVE[sub_counter] = deep_learning_df["AUG_INDEX"][ANCHOR[sub_counter]]
            #     POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = deep_learning_df["AUG_IMG_URL"][ANCHOR[sub_counter]]

    nested_list=[VARIANT_ID, ANCHOR, ANCHOR_CURATOR_IMAGE_PATH,
                   POSITIVE, POSITIVE_CURATOR_IMAGE_PATH]

    data_list=np.array(nested_list).T.tolist()

    df_triplet=pd.DataFrame(data_list, columns=["VARIANT_ID", "ANCHOR", "ANCHOR_CURATOR_IMAGE_PATH",
                                                  "POSITIVE", "POSITIVE_CURATOR_IMAGE_PATH"])

    # Load the table with negative examples
    deep_learning_index_n=pd.read_csv(cwd + output_table_path_6)

    # Count the number of items including the seed + similar items
    counter_item_n=int((num_items - 1) / 2)
    # for j in range(len(deep_learning_index.loc[0]) - 1):
    #     counter_item += 1

    # Convert the table with negative examples to another table with two columns;
    # the first column is the anchor item and the second column is the
    # negative item
    jj=0
    df_triplet_temp_n=np.zeros(
        (len(deep_learning_index_n) * counter_item_n, 2)).astype(int)
    for i in range(len(deep_learning_index_n)):
        for j in range(counter_item):
            df_triplet_temp_n[jj, 0]=int(deep_learning_index_n.loc[i][1])
            df_triplet_temp_n[jj, 1]=int(
                deep_learning_index_n.loc[i][1 + j + 1])
            # if j == 0:
            #     df_triplet_temp_n[jj, 1] = int(
            #         deep_learning_index_n.loc[i][1 + j + 1])
            jj += 1

    df_triplet_temp_n_2 = copy.deepcopy(df_triplet_temp_n)       

    # print(df_triplet_temp_n)

    # VARIANT_ID_n=[]
    # ANCHOR_n=[]
    # ANCHOR_CURATOR_IMAGE_PATH_n=[]
    # POSITIVE_n=[]
    # POSITIVE_CURATOR_IMAGE_PATH_n=[]
    # for i in range(len(df_triplet_temp_n)):
    #     VARIANT_ID_n.append(deep_learning_df["VARIANT_ID"][
    #                         df_triplet_temp_n[i, 0]])
    #     ANCHOR_n.append(int(df_triplet_temp_n[i, 0]))
    #     ANCHOR_CURATOR_IMAGE_PATH_n.append(
    #         deep_learning_df[option][df_triplet_temp_n[i, 0]])
    #     if df_triplet_temp_n[i, 1] != -1:
    #         POSITIVE_n.append(int(df_triplet_temp_n[i, 1]))
    #         POSITIVE_CURATOR_IMAGE_PATH_n.append(
    #             deep_learning_df[option][df_triplet_temp_n[i, 1]])
    #     else:
    #         POSITIVE_n.append(55025)
    #         POSITIVE_CURATOR_IMAGE_PATH_n.append(
    #             deep_learning_df[option][55025])

    counter_item = counter_item_n
    df_triplet_temp = df_triplet_temp_n
    VARIANT_ID = []
    ANCHOR = []
    ANCHOR_CURATOR_IMAGE_PATH = []
    POSITIVE = []
    POSITIVE_CURATOR_IMAGE_PATH = []
    negative_sub_all = []
    no_vs_items = int(len(df_triplet_temp) / counter_item)
    for i in range(no_vs_items):
        positive_sub_counter = []
        for sub_i in range(counter_item):
            sub_counter = i * counter_item + sub_i
            VARIANT_ID.append(deep_learning_df["VARIANT_ID"][
                              df_triplet_temp[sub_counter, 0]])
            ANCHOR.append(int(df_triplet_temp[sub_counter, 0]))
            ANCHOR_CURATOR_IMAGE_PATH.append(
                deep_learning_df[option][df_triplet_temp[sub_counter, 0]])

            if df_triplet_temp[sub_counter, 1] != -1:
                POSITIVE.append(int(df_triplet_temp[sub_counter, 1]))
                POSITIVE_CURATOR_IMAGE_PATH.append(
                    deep_learning_df[option][df_triplet_temp[sub_counter, 1]])
            else:
                POSITIVE.append(int(df_triplet_temp[sub_counter, 0]))
                POSITIVE_CURATOR_IMAGE_PATH.append(
                    deep_learning_df[option][df_triplet_temp[sub_counter, 0]])

            if df_triplet_temp[sub_counter, 1] != -1:
                positive_sub_counter.append(sub_counter)

        negative_sub_all.append(positive_sub_counter)        

        for sub_i in range(counter_item):
            sub_counter = i * counter_item + sub_i
            if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter != []:
                 
                random_positive = random.choice(positive_sub_counter)
                POSITIVE[sub_counter] = (
                    int(df_triplet_temp[random_positive, 1]))
                POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = (
                    deep_learning_df[option][df_triplet_temp[random_positive, 1]])

            if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter == []:
                # POSITIVE[sub_counter] = ANCHOR[sub_counter]
                # POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = deep_learning_df["CURATOR_IMAGE_PATH"][ANCHOR[sub_counter]]
                POSITIVE[sub_counter] = 155025
                POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = " "  

            # if df_triplet_temp[sub_counter, 1] == -1 and positive_sub_counter == []:
            #     POSITIVE[sub_counter] = deep_learning_df["AUG_INDEX"][ANCHOR[sub_counter]]
            #     POSITIVE_CURATOR_IMAGE_PATH[sub_counter] = deep_learning_df["AUG_IMG_URL"][ANCHOR[sub_counter]]

    VARIANT_ID_n = VARIANT_ID
    ANCHOR_n = ANCHOR
    ANCHOR_CURATOR_IMAGE_PATH_n = ANCHOR_CURATOR_IMAGE_PATH
    POSITIVE_n = POSITIVE
    POSITIVE_CURATOR_IMAGE_PATH_n = POSITIVE_CURATOR_IMAGE_PATH   

    nested_list_n=[VARIANT_ID_n, ANCHOR_n, ANCHOR_CURATOR_IMAGE_PATH_n,
                    POSITIVE_n, POSITIVE_CURATOR_IMAGE_PATH_n]        

    data_list_n=np.array(nested_list_n).T.tolist()

    df_triplet_n=pd.DataFrame(data_list_n, columns=["VARIANT_ID", "ANCHOR", "ANCHOR_CURATOR_IMAGE_PATH",
                                                      "POSITIVE", "POSITIVE_CURATOR_IMAGE_PATH"])

    # Prepare the table with the negative examples
    df_triplet["NEGATIVE"]=df_triplet_n["POSITIVE"]
    df_triplet["NEGATIVE_CURATOR_IMAGE_PATH"]=df_triplet_n[
        "POSITIVE_CURATOR_IMAGE_PATH"]

    # df_triplet["VARIANT_ID"] = df_triplet["VARIANT_ID"].astype('int64')

    # Merge the positive and negative tables
    df_triplet_final=pd.merge(
        df_triplet, deep_learning_df, on='VARIANT_ID', how='left')

    # Pull the columns needed
    df_triplet_final=df_triplet_final[["VARIANT_ID",
                                         "ANCHOR", "ANCHOR_CURATOR_IMAGE_PATH",
                                         "POSITIVE", "POSITIVE_CURATOR_IMAGE_PATH",
                                         "NEGATIVE", "NEGATIVE_CURATOR_IMAGE_PATH",
                                         "STYLES", "TYPES", "CATEGORIES"]]

    # df_triplet_final=df_triplet_final[
    #     df_triplet_final["CATEGORIES"] == "jewelry"]

    # df_triplet_final=df_triplet_final[
    #      df_triplet_final["TYPES"] == "pendantnecklace"]

    indices = np.random.permutation(len(df_triplet_final))              

    df_triplet_final = df_triplet_final.loc[indices]

    print("len(df_triplet_final) = ", len(df_triplet_final))
    # print(df_triplet_final.head(50))
    # print(df_triplet_final.loc[:50])
    # print(df_triplet_final["NEGATIVE"][:50])
    # print(df_triplet_final["NEGATIVE"][4])
    # print(df_triplet_final["NEGATIVE"][0])  
    # print(type(df_triplet_final["NEGATIVE"][0]))     

    df_triplet_final = df_triplet_final[df_triplet_final["NEGATIVE"] != "155025"] 

    print("len(df_triplet_final) = ", len(df_triplet_final)) 

    df_triplet_final = df_triplet_final[df_triplet_final["POSITIVE"] != "155025"] 

    print("len(df_triplet_final) = ", len(df_triplet_final))
    # df_triplet_final=df_triplet_final.head(640)
    # print(len(df_triplet_final))
    # print(df_triplet_final["VARIANT_ID"].head(40))
    # print(df_triplet_final["ANCHOR"].head(40))
    # print(df_triplet_final["POSITIVE"].head(40))
    # print(df_triplet_final["NEGATIVE"].head(40))
    # print(df_triplet_final["ANCHOR_CURATOR_IMAGE_PATH"].head(40))
    # print(df_triplet_final["POSITIVE_CURATOR_IMAGE_PATH"].head(40))
    # print(df_triplet_final["NEGATIVE_CURATOR_IMAGE_PATH"].head(40))

    # print(deep_learning_df["IMAGE_URL"][55025])
    # print(deep_learning_df["VARIANT_ID"][55025])

    # test_table=pd.DataFrame(df_triplet_final)
    # test_table.to_csv(cwd + "/output_tables/" + "test_tables.csv")

    # print("455" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][455])
    # print("5288" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][5288])
    # print("9854" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][9854])
    # print("537" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][537])
    # print("2801" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][2801])
    # print("539" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][539])
    # print(df_triplet_final.head(20))

    # with open(cwd + output_table_path_4, "wb") as my_file:
    #     pickle.dump(df_triplet_final, my_file, protocol=2)
    
    print("******************************************************************")   

    count_posi = 0
    for i in range(len(positive_sub_all)):
        if len(positive_sub_all[i]) == 0:
            count_posi += 1
    count_nega = 0        
    for i in range(len(negative_sub_all)):
        if len(negative_sub_all[i]) == 0:
            count_nega += 1 
    print("count_posi = ", count_posi) 
    print("count_nega = ", count_nega)               
    print("len(positive_sub_all) = ", len(positive_sub_all))
    print("len(negative_sub_all) = ", len(negative_sub_all))
    print("no_vs_items = ", no_vs_items) 
    print("len(ANCHOR) = ", len(ANCHOR))
    print("len(VARIANT_ID) = ", len(VARIANT_ID)) 

    ANCHOR_small = []
    for i in range(len(ANCHOR)):
        if i%20 == 0:
            ANCHOR_small.append(ANCHOR[i])

    print("len(ANCHOR_small) = ", len(ANCHOR_small))

    VARIANT_ID_small = []
    for i in range(len(VARIANT_ID)):
        if i%20 == 0:
            VARIANT_ID_small.append(VARIANT_ID[i])

    print("len(VARIANT_ID_small) = ", len(VARIANT_ID_small))        

    # df_triplet_temp_2
    VARIANT_ID_2 = []
    ANCHOR_2 = []
    ANCHOR_CURATOR_IMAGE_PATH_2 = []
    POSITIVE_2 = []
    POSITIVE_CURATOR_IMAGE_PATH_2 = []
    NEGATIVE_2 = []
    NEGATIVE_CURATOR_IMAGE_PATH_2 = []
    for i in range(no_vs_items):
        no_batch_items = len(positive_sub_all[i]) * len(negative_sub_all[i])
        for j in range(len(positive_sub_all[i])):
            for jj in range(len(negative_sub_all[i])):
                POSITIVE_2.append((int(df_triplet_temp_2[positive_sub_all[i][j], 1])))
                POSITIVE_CURATOR_IMAGE_PATH_2.append(deep_learning_df[option][df_triplet_temp_2[positive_sub_all[i][j], 1]])

                NEGATIVE_2.append((int(df_triplet_temp_n_2[negative_sub_all[i][jj], 1])))
                NEGATIVE_CURATOR_IMAGE_PATH_2.append(deep_learning_df[option][df_triplet_temp_n_2[negative_sub_all[i][jj], 1]])

                anchor_index = ANCHOR_small[i]
                ANCHOR_2.append(anchor_index)
                ANCHOR_CURATOR_IMAGE_PATH_2.append(deep_learning_df[option][anchor_index])

                anchor_id = VARIANT_ID_small[i]
                VARIANT_ID_2.append(anchor_id)

    # print(len(VARIANT_ID_2)) 
    print("len(ANCHOR_2) = ", len(ANCHOR_2)) 
    # print(len(ANCHOR_CURATOR_IMAGE_PATH_2)) 
    # print(len(POSITIVE_2)) 
    # print(len(POSITIVE_CURATOR_IMAGE_PATH_2)) 
    # print(len(NEGATIVE_2)) 
    # print(len(NEGATIVE_CURATOR_IMAGE_PATH_2)) 

    nested_list_2=[VARIANT_ID_2, ANCHOR_2, ANCHOR_CURATOR_IMAGE_PATH_2,
                    POSITIVE_2, POSITIVE_CURATOR_IMAGE_PATH_2, NEGATIVE_2, NEGATIVE_CURATOR_IMAGE_PATH_2]        

    data_list_2=np.array(nested_list_2).T.tolist()

    df_triplet_2=pd.DataFrame(data_list_2, columns=["VARIANT_ID", "ANCHOR", "ANCHOR_CURATOR_IMAGE_PATH",
                                                      "POSITIVE", "POSITIVE_CURATOR_IMAGE_PATH",
                                                      "NEGATIVE", "NEGATIVE_CURATOR_IMAGE_PATH"])

    # Merge the positive and negative tables
    df_triplet_final_2=pd.merge(
        df_triplet_2, deep_learning_df, on='VARIANT_ID', how='left')

    # Pull the columns needed
    df_triplet_final_2=df_triplet_final_2[["VARIANT_ID",
                                         "ANCHOR", "ANCHOR_CURATOR_IMAGE_PATH",
                                         "POSITIVE", "POSITIVE_CURATOR_IMAGE_PATH",
                                         "NEGATIVE", "NEGATIVE_CURATOR_IMAGE_PATH",
                                         "STYLES", "TYPES", "CATEGORIES"]]

    # df_triplet_final_2=df_triplet_final_2[
    #     df_triplet_final_2["CATEGORIES"] == "jewelry"]

    print("len(df_triplet_final_2) = ", len(df_triplet_final_2))     

    df_triplet_final_2 = df_triplet_final_2[df_triplet_final_2["NEGATIVE"] != "155025"] 

    print("len(df_triplet_final_2) = ", len(df_triplet_final_2)) 

    df_triplet_final_2 = df_triplet_final_2[df_triplet_final_2["POSITIVE"] != "155025"] 

    print("len(df_triplet_final_2) = ", len(df_triplet_final_2))
    # df_triplet_final_2=df_triplet_final_2.head(6400)
    # df_triplet_final_2=df_triplet_final_2.head(37120)
    # print(len(df_triplet_final_2))

    # test_table=pd.DataFrame(df_triplet_final_2)
    # test_table.to_csv(cwd + "/output_tables/" + "test_tables.csv")

    # with open(cwd + "/output_tables/deep_learning_df_aug", "rb") as my_file4:
    # # with open(cwd + "/output_tables/deep_learning_df_aug", "rb") as my_file2:    
    #     # deep_learning_df = pickle.load(my_file2,encoding='latin1')
    #     deep_learning_df_aug = pickle.load(my_file4)

    # i = 0
    # startTime = datetime.now()
    # i_counter = len(df_triplet_final_2)
    # df_triplet_final_aug_2 = pd.DataFrame({"VARIANT_ID": ["DUMMY"], "ANCHOR": ["DUMMY"],
    #                                        "ANCHOR_CURATOR_IMAGE_PATH": ["DUMMY"],
    #                                        "POSITIVE": ["DUMMY"], "POSITIVE_CURATOR_IMAGE_PATH": ["DUMMY"],
    #                                        "NEGATIVE": ["DUMMY"], "NEGATIVE_CURATOR_IMAGE_PATH": ["DUMMY"],
    #                                        "STYLES": ["DUMMY"], "TYPES": ["DUMMY"], "CATEGORIES": ["DUMMY"]})
    
    # for item in df_triplet_final_2.index.tolist():
    #     if i < 5000:
    #         index_anchor = df_triplet_final_2.loc[item]["ANCHOR"]
    #         index_positive = df_triplet_final_2.loc[item]["POSITIVE"]
    #         index_negative = df_triplet_final_2.loc[item]["NEGATIVE"]

    #         id_anchor = deep_learning_df.loc[int(index_anchor)]["VARIANT_ID"]
    #         id_positive = deep_learning_df.loc[int(index_positive)]["VARIANT_ID"] 
    #         id_negative = deep_learning_df.loc[int(index_negative)]["VARIANT_ID"]  

    #         index_aug_anchor = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_anchor]["AUG_INDEX"].values[0]
    #         index_aug_positive = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_positive]["AUG_INDEX"].values[0]
    #         index_aug_negative = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_negative]["AUG_INDEX"].values[0]

    #         img_aug_anchor = deep_learning_df_aug.loc[index_aug_anchor]["AUG_IMG_PATH"] 
    #         img_aug_positive = deep_learning_df_aug.loc[index_aug_positive]["AUG_IMG_PATH"] 
    #         img_aug_negative = deep_learning_df_aug.loc[index_aug_negative]["AUG_IMG_PATH"] 

    #         id_aug_anchor = deep_learning_df_aug.loc[index_aug_anchor]["VARIANT_ID"] 
    #         id_aug_positive = deep_learning_df_aug.loc[index_aug_positive]["VARIANT_ID"] 
    #         id_aug_negative = deep_learning_df_aug.loc[index_aug_negative]["VARIANT_ID"]

    #         if "/" not in id_aug_anchor and "/" not in id_aug_positive and  "/" not in id_aug_negative:

    #             items = df_triplet_final_2.loc[item]
                
    #             df2 = items.copy(deep=True) 
    #             df2["ANCHOR"] = str(index_aug_anchor)
    #             df2["ANCHOR_CURATOR_IMAGE_PATH"] = img_aug_anchor
    #             df2["POSITIVE"] = str(index_aug_positive)
    #             df2["POSITIVE_CURATOR_IMAGE_PATH"] = img_aug_positive
    #             df2["NEGATIVE"] = str(index_aug_negative)
    #             df2["NEGATIVE_CURATOR_IMAGE_PATH"] = img_aug_negative 

    #             df_triplet_final_aug_2 = df_triplet_final_aug_2.append(df2, ignore_index=True) 

    #     i += 1 
    #     # if i % 500 == 0:
    #     #     time_used = datetime.now() - startTime
    #     #     print("ITEM: " + str(i+1) + " IS FINISHED")

    # df_triplet_final_aug_2 = df_triplet_final_aug_2[df_triplet_final_aug_2["VARIANT_ID"] != "DUMMY"]  

    # df_triplet_final_2_train = df_triplet_final_2.head(3840)
    # df_triplet_final_2_train = df_triplet_final_2_train.sample(frac=1)

    # df_triplet_final_2_train_aug = df_triplet_final_aug_2.head(27360)
    # # df_triplet_final_2_train_aug = df_triplet_final_2_train_aug.sample(frac=1)

    # df_triplet_final_3 = df_triplet_final_2_train.append(df_triplet_final_2_train_aug, ignore_index=True)
    # df_triplet_final_3 = df_triplet_final_3.sample(frac=1)

    # df_triplet_final_3 = df_triplet_final_3.head(27360)

    # df_triplet_final_2_test = df_triplet_final_2.tail(960)
    # df_triplet_final_2_test = df_triplet_final_2_test.sample(frac=1)

    # df_triplet_final_3 = df_triplet_final_2_train.append(df_triplet_final_2_test, ignore_index=True)

    if train_or_test == "test":
        df_triplet_final_2 = df_triplet_final_2.sample(frac=1).reset_index(drop=True)
    
    df_triplet_final_3 = df_triplet_final_2

    # df_triplet_final_3 = df_triplet_final_3.append(df_triplet_final_2_test, ignore_index=True)

    # # df_triplet_final_2 = df_triplet_final_2.sample(frac=1)

    # test_table=pd.DataFrame(df_triplet_final_2)
    # test_table.to_csv(cwd + "/output_tables/" + "test_tables.csv")

    # test_table=pd.DataFrame(df_triplet_final_3)
    # test_table.to_csv(cwd + "/output_tables/" + "test_tables_11202018.csv")

    # test_table=pd.DataFrame(df_triplet_final_aug_2)
    # test_table.to_csv(cwd + "/output_tables/" + "test_tables_aug.csv")

    # # print("455" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][455])
    # # print("5288" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][5288])
    # # print("9854" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][9854])
    # # print("537" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][537])
    # # print("2801" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][2801])
    # # print("539" + " " + deep_learning_df["CURATOR_IMAGE_PATH"][539])
    # # print(df_triplet_final.head(20))

    with open(cwd + output_table_path_4, "wb") as my_file:
        pickle.dump(df_triplet_final_3, my_file, protocol=2)
          
    print("******************************************************************")     

    # i = 0
    # i_counter = len(df_triplet_final_2)
    # for item in df_triplet_final_2["VARIANT_ID"]:
    #     if i < 2:
    #         print(item)
    #         print(df_triplet_final_2["VARIANT_ID"] == item)
    #         index_anchor = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["ANCHOR"].values[0]
    #         index_positive = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["POSITIVE"].values[0]
    #         index_negative = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["NEGATIVE"].values[0]

    #         # img_path_anchor = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["ANCHOR"].values[0]
    #         # img_path_positive = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["ANCHOR_CURATOR_IMAGE_PATH"].values[0]
    #         # img_path_negative = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item]["NEGATIVE_CURATOR_IMAGE_PATH"].values[0]

    #         print(index_anchor)
    #         id_anchor = deep_learning_df_original.loc[int(index_anchor)]["VARIANT_ID"]
    #         id_positive = deep_learning_df_original.loc[int(index_positive)]["VARIANT_ID"] 
    #         id_negative = deep_learning_df_original.loc[int(index_negative)]["VARIANT_ID"]  

    #         index_aug_anchor = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_anchor]["AUG_INDEX"].values[0]
    #         index_aug_positive = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_positive]["AUG_INDEX"].values[0]
    #         index_aug_negative = deep_learning_df_aug[deep_learning_df_aug["VARIANT_ID"] == id_negative]["AUG_INDEX"].values[0]

    #         img_aug_anchor = deep_learning_df_aug.loc[index_aug_anchor]["AUG_IMG_PATH"] 
    #         img_aug_positive = deep_learning_df_aug.loc[index_aug_positive]["AUG_IMG_PATH"] 
    #         img_aug_negative = deep_learning_df_aug.loc[index_aug_negative]["AUG_IMG_PATH"] 

    #         items = df_triplet_final_2[df_triplet_final_2["VARIANT_ID"] == item] 

    #         df2 = items.copy(deep=True) 
    #         df2["ANCHOR"] = index_aug_anchor
    #         df2["ANCHOR_CURATOR_IMAGE_PATH"] = img_aug_anchor
    #         df2["POSITIVE"] = index_aug_positive
    #         df2["POSITIVE_CURATOR_IMAGE_PATH"] = img_aug_positive 
    #         df2["NEGATIVE"] = index_aug_negative
    #         df2["NEGATIVE_CURATOR_IMAGE_PATH"] = img_aug_negative 

    #         if i == 0:
    #             df_triplet_final_aug_2 = df2.copy(deep=True) 

    #         else:
    #             df_triplet_final_aug_2 = df_triplet_final_aug_2.append(df2, ignore_index=True) 

    #         i += 1 
           
