import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

from . import generatorre as generator
from . import modelsre as models
from . import searchre as search

# logging.basicConfig(level=logging.INFO)


def embedding(model_input_path,
              n_H0, n_W0, n_C0,
              batch_size, training_ratio, embedding_size,
              search_size, search_size_2, image_normalization,
              same_image_epsilon,
              model_weight_path,
              output_table_path_2,
              model_name,
              output_layer,
              output_table_path_9,
              keepratio):
    """

    """

    # Import the model input table
    with open(model_input_path, "rb") as my_file2:
        deep_learning_df = pickle.load(my_file2)

    # Load the model (the pre-trained part) weights
    cwd = os.getcwd()
    model_weight_path_update = cwd + model_weight_path

    # deep_learning_df[deep_learning_df["VARIANT_ID"] == ""]

    # Group by type and/or style
    # grouped = deep_learning_df.groupby(['TYPES'])
    grouped = deep_learning_df.groupby(['CATEGORIES'])
    logging.basicConfig(level=logging.INFO)
    logging.info("************************************")
    logging.info("***NUMBER OF LSBS:" + str(len(deep_learning_df)))
    logging.info("***NUMBER OF GROUPS:" + str(len(grouped)))
    logging.info("************************************")
    # print("NUMBER OF GROUPS:" + str(len(grouped)))

    # Start the tensorflow process
    init = tf.global_variables_initializer()
    with tf.Session() as sess:

        sess.run(init)

        if model_name == "vgg19":

            # Load the weights of the model
            img_placeholder = tf.placeholder(
                tf.float32, shape=(None, n_H0, n_W0, n_C0))
            net_val, mean_pixel = models.vgg19_no_top(
                model_weight_path_update, img_placeholder)

            # train_features = net_val['relu7']
            # train_features_2 = tf.contrib.layers.flatten(train_features)
            # train_features_3 = tf.nn.l2_normalize(train_features_2, dim=1)

        elif model_name == "triplet":

            new_saver = tf.train.import_meta_graph(
                cwd + "/weights/" + 'my_test_model.meta')
            new_saver.restore(
                sess, tf.train.latest_checkpoint(cwd + "/weights/"))
            graph = tf.get_default_graph()

            # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]

            # print(tensors)

            img_placeholder_a = graph.get_tensor_by_name(
                "img_placeholder_a:0")
            train_features_aaa = graph.get_tensor_by_name(
                "train_features_aaa:0")
            keepratio_tf = graph.get_tensor_by_name(
                "keepratio_tf:0")

        # We process each group one by one
        # key_i is the index used for tracking the group
        # total is used to count the total number of items processed
        # total_bad_imgs are used to track the empty jpg files
        # the visually similar result is stored in the dictionary
        # deep_learning_dic
        key_i = 0
        total = 0
        total_bad_imgs = 0
        deep_learning_dic = {}
        deep_learning_score_dic = {}
        for name, group_i in grouped:

            # We only consider the groups having at least two items
            if len(group_i) >= 2:
                # if name in list_groups:

                # we use the index of the table to find the corresponding item
                index = group_i.index.values

                # Load the image directory (absolute path)
                image_path = group_i["CURATOR_IMAGE_PATH"]

                # y_lables is not used actually, so please ignore it (it is used
                # for classificartion task)
                y_labels = np.zeros((len(index), 1))
                # print("***NUMBER OF SAMPLES:" + str(len(y_labels)))
                total += len(y_labels)
                # print("***TOTAL NUMBER OF SAMPLES:" + str(total))
                logging.basicConfig(level=logging.INFO)
                logging.info("***GROUP" + " " + str(key_i) + " " + "STRATS")
                logging.info("***NUMBER OF SAMPLES:" + str(len(y_labels)))
                logging.info("***TOTAL NUMBER OF SAMPLES SO FAR:" + str(total))

                number_of_products = len(y_labels)

                # Parameters
                params = {'dim_x': n_W0,
                          'dim_y': n_H0,
                          'dim_z': n_C0,
                          'batch_size': batch_size,
                          'image_normalization': image_normalization,
                          'n_y': 1,
                          'shuffle': False}

                y_labels_training = {}
                partition_training = []
                for i in range(0, int(number_of_products * training_ratio)):
                    index_i = int(index[i])
                    y_labels_training[str(index_i)] = y_labels[i, :]
                    partition_training.append(str(index_i))

                # This generator will generate images in batch
                training_generator = generator.DataGenerator(
                    **params).generate(y_labels_training, partition_training, image_path)

                num_batches = np.int(np.round(number_of_products / batch_size))
                matrix_res = np.zeros(
                    [np.int(np.round(number_of_products)), embedding_size])

                startTime = datetime.now()
                for i in range(num_batches):

                    (batch_X_training, batch_Y_training,
                     bad_img) = next(training_generator)
                    total_bad_imgs += bad_img

                    if model_name == "vgg19":
                        train_features = net_val[output_layer].eval(
                            feed_dict={img_placeholder: batch_X_training})

                        # train_vectorized = train_features_3.eval(
                        #     feed_dict={img_placeholder: batch_X_training})

                        train_vectorized = np.ndarray(
                            (batch_size, embedding_size))
                        for j in range(batch_size):
                            curr_feat = train_features[j, :, :, :]
                            curr_feat_vec = np.reshape(curr_feat, (1, -1))
                            train_vectorized[j, :] = curr_feat_vec

                        matrix_res[
                            i * batch_size:(i + 1) * batch_size, :] = train_vectorized

                    elif model_name == "triplet":
                        train_features_aaa_np = train_features_aaa.eval(
                            feed_dict={img_placeholder_a: batch_X_training, keepratio_tf: keepratio})

                        train_vectorized_aaa = np.ndarray(
                            (batch_size, embedding_size))
                        for j in range(batch_size):
                            curr_feat_aaa = train_features_aaa_np[j, :]
                            curr_feat_vec_aaa = np.reshape(
                                curr_feat_aaa, (1, -1))
                        train_vectorized_aaa[j, :] = curr_feat_vec_aaa

                        matrix_res[
                            i * batch_size:(i + 1) * batch_size, :] = train_vectorized_aaa

                time_used = datetime.now() - startTime
                logging.basicConfig(level=logging.INFO)
                logging.info(
                    "***TOTAL NUMBER OF BAD IMAGES SO FAR:" + str(total_bad_imgs))
                logging.info("***TIME TAKEN:" + str(time_used))
                # print("***TOTAL NUMBER OF BAD IMAGES:" + str(total_bad_imgs))
                # print("***TIME TAKEN:" + str(time_used))

                # print("************************************************************************************")
                sim = matrix_res.dot(matrix_res.T)
                sim += 0.0000000001
                norms = np.array([np.sqrt(np.diagonal(sim))])
                # print(norms)
                # print(len(norms[0]))
                j = 0
                for i in norms[0]:
                    if i == 0:
                        print(j)
                        print(i)
                    j +=1    
                sim = sim / norms / norms.T
                # print("************************************************************************************")

                # print("***MIN SIM:" + str(np.min(sim)))

                number_of_products_2 = len(matrix_res)
                idx_to_product = list(range(number_of_products_2))

                jj = 0
                deep_learning_output = {}
                deep_learning_score = {}
                for idx in range(len(idx_to_product)):
                    products_temp = search.top_k_products(
                        sim, idx_to_product, idx,
                        search_size, search_size_2,
                        same_image_epsilon)

                    sim_idx = sim[idx, :]

                    products_2, sim_score = search.top_k_products_parser(
                        idx, search_size_2, products_temp,
                        same_image_epsilon, sim_idx, index)

                    deep_learning_output[str(jj)] = products_2
                    deep_learning_score[str(jj)] = sim_score
                    jj += 1

                deep_learning_dic[str(key_i)] = deep_learning_output
                deep_learning_score_dic[str(key_i)] = deep_learning_score

                logging.basicConfig(level=logging.INFO)
                logging.info("***GROUP" + " " + str(key_i) + " " + "COMPLETED")

                # print("***GROUP" + " " + str(key_i) + " " + "COMPLETED")

                key_i += 1

                # tf.reset_default_graph()

    cwd = os.getcwd()

    model_output_path = cwd + output_table_path_2
    with open(model_output_path, "wb") as my_file:
        pickle.dump(deep_learning_dic, my_file, protocol=2)

    model_output_path_2 = cwd + output_table_path_9
    with open(model_output_path_2, "wb") as my_file2:
        pickle.dump(deep_learning_score_dic, my_file2, protocol=2)    

    return model_output_path, model_output_path_2
