import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf


# Compute the Euclidean distance between two vectors
def compute_euclidean_distance(x, y):
    d = tf.square(tf.subtract(x, y))
    d = tf.sqrt(tf.reduce_sum(d, 1))
    return d

# Compute the triplet cost
def compute_triplet_loss(anchor_feature, positive_feature, negative_feature, margin):
    with tf.name_scope("triplet_loss"):
        d_p_squared = tf.square(compute_euclidean_distance(
            anchor_feature, positive_feature))
        d_n_squared = tf.square(compute_euclidean_distance(
            anchor_feature, negative_feature))
        loss = tf.maximum(0., d_p_squared - d_n_squared + margin)
    return tf.reduce_mean(loss), tf.reduce_mean(d_p_squared), tf.reduce_mean(d_n_squared)
