import sys

# TODO: use python argparse to manage the configuration file
# from command line

defaults_config = {"n_H0": 112,
                   "n_W0": 112,
                   "n_C0": 3,
                   "batch_size": 1,
                   "training_ratio": 1.00,
                   "embedding_size": 4096,
                   # "embedding_size": 25088,
                   # "embedding_size": 100352,
                   "net_3_dim": 4096,
                   "search_size": 31,
                   "search_size_2": 21,
                   "image_normalization": 250.,
                   "same_image_epsilon": 0.00000001,
                   "num_epoch": 3,
                   "MARGIN": 10,
                   "learning_rate": 0.0001,
                   "keepratio": 1.0,
                   # "model_name": "vgg19",
                   "model_name": "triplet",
                   # "output_layer": "relu7",
                   "output_layer": "relu5_4",
                   # "output_layer": "pool5",
                   # "database_path": sys.argv[1],
                   # "database_path": "/data_tables/VISUALLY_SIMILAR_NM_QA_APPROVED_EXTENDED_20181212_CV.csv",
                   # "database_path": "/data_tables/VISUALLY_SIMILAR_NM_APPROVED_EXTENDED_20190610_CV.csv",
                   # "database_path": "/data_tables/VISUALLY_SIMILAR_NM_APPROVED_EXTENDED_07302019.csv",
                   "database_path": "/data_tables/VISUALLY_SIMILAR_Target_APPROVED_EXTENDED_07262019.csv",
                   # "database_path": "/data_tables/VISUALLY_SIMILAR_Target_APPROVED_EXTENDED_07302019.csv",
                   "output_table_path": "/output_tables/deep_learning_df",
                   "output_table_path_2": "/output_tables/deep_learning_dic",
                   "output_table_path_3": "/output_tables/",
                   "output_table_path_4": "/output_tables/df_triplet_final",
                   "output_table_path_5": "/output_tables/test_p.csv",
                   "output_table_path_6": "/output_tables/test_n.csv",
                   "output_table_path_7": "/data_tables/test_box_reference.csv",
                   "output_table_path_8": "/data_tables/test_box.csv",
                   "output_table_path_9": "/output_tables/deep_learning_score_dic",
                   "output_table_path_10": "/output_tables/test_p_2.csv",
                   "output_table_path_11": "/output_tables/test_n_2.csv",
                   "output_table_path_12": "/output_tables/retraining_bed_target_id_score_0726201925088.json",
                   # "output_table_path_13": "/output_tables/[NM]approvedAndUnapprovedVSI_1556010313238.csv",
                   # "output_table_path_13": "/output_tables/[NM]approvedAndUnapprovedVSI_1560175286288.csv",
                   # "output_table_path_13": "/output_tables/[nm]approvedAndUnapprovedVSI_1564485238688.csv",
                   "output_table_path_13": "/output_tables/[target]approvedAndUnapprovedVSI_1564485349001.csv",
                   "model_weight_path": "/weights/imagenet-vgg-verydeep-19.mat",
                   "model_weight_path_2": "/weights/",
                   "column_names": ["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES"]
                   }


def parse_arguments():
    args = defaults_config
    return args
