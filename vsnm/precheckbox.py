import csv
import numpy as np
import os
import pandas as pd
import re
import pickle


def parse_data(output_table_path,
               output_table_path_7,
               output_table_path_8,
               output_table_path_13,
               search_size_2):

    # num_items = (search_size_2 - 1) * 2 + 1

    cwd = os.getcwd()
    csv_path = output_table_path_13
    test_box_all = pd.read_csv(cwd + csv_path)

    def parser(str_input):
        return str(str_input)        

    # Apply the parsers to the types and styles
    test_box_all["Seed Item Selected"] = test_box_all.apply(lambda row: parser(row["Seed Item Selected"]), axis=1)
     

    print("test_box_all.columns.values = ", len(test_box_all.columns.values))
    print("len(test_box_all) = ", len(test_box_all))
    print(test_box_all.head(5))

    num_items = len(test_box_all.columns.values)
    num_items = 41

    test_box_all = test_box_all[["Seed Item Selected",
                                 'Item 1 - Positive', 'Item 1 - Negative',
                                 'Item 2 - Positive', 'Item 2 - Negative',
                                 'Item 3 - Positive', 'Item 3 - Negative',
                                 'Item 4 - Positive', 'Item 4 - Negative',
                                 'Item 5 - Positive', 'Item 5 - Negative',
                                 'Item 6 - Positive', 'Item 6 - Negative',
                                 'Item 7 - Positive', 'Item 7 - Negative',
                                 'Item 8 - Positive', 'Item 8 - Negative',
                                 'Item 9 - Positive', 'Item 9 - Negative',
                                 'Item 10 - Positive', 'Item 10 - Negative',
                                 'Item 11 - Positive', 'Item 11 - Negative',
                                 'Item 12 - Positive', 'Item 12 - Negative',
                                 'Item 13 - Positive', 'Item 13 - Negative',
                                 'Item 14 - Positive', 'Item 14 - Negative',
                                 'Item 15 - Positive', 'Item 15 - Negative',
                                 'Item 16 - Positive', 'Item 16 - Negative',
                                 'Item 17 - Positive', 'Item 17 - Negative',
                                 'Item 18 - Positive', 'Item 18 - Negative',
                                 'Item 19 - Positive', 'Item 19 - Negative',
                                 'Item 20 - Positive', 'Item 20 - Negative']]

    test_box_all = test_box_all.fillna("Missing")
    test_box_all_copy = test_box_all.copy(deep=True)

    # print(test_box_all_copy[:1])
    # print(test_box_all_copy.loc[0])
    # print(test_box_all_copy.loc[0][1])
    # Seed = test_box_all["Seed Item Selected"]
    # print(Seed)
    # print(Seed[0])
    # print(Seed[1])
    # indices = np.random.permutation(len(Seed))
    # Seed = Seed.loc[indices]
    # print(Seed)
    # print(Seed[0])
    # print(Seed[1])

    for i in range(0, len(test_box_all)):
        for j in range(0, num_items):
            if j == 0:
                test_box_all_copy.loc[i][j] = "TRUE"
            elif (j > 0 and test_box_all.loc[i][j] != "Missing"):
                test_box_all_ij = test_box_all.loc[i][j]
                test_box_all_true_false = re.split('#', test_box_all_ij)[2]
                test_box_all_copy.loc[i][j] = test_box_all_true_false     

    test_box_all_copy = test_box_all_copy.replace("Missing", "FALSE")
    test_box_all_copy = test_box_all_copy.replace("FALSE", False)
    test_box_all_copy = test_box_all_copy.replace("TRUE", True)
    test_box = test_box_all_copy

    test_box.to_csv(cwd + output_table_path_8, index=False)
    print(test_box.head(5))

    model_input_path = output_table_path
    with open(cwd + model_input_path, "rb") as my_file:
        deep_learning_df = pickle.load(my_file)

    test_box_reference = (
        np.ones((len(test_box_all), num_items)).astype(int)) * -1
    missing_items = []
    ii = []
    for i in range(0, len(test_box_all)):
        for j in range(num_items):
            if j == 0:
                test_box_all_ij = test_box_all.loc[i][j]
                test_box_all_ij_id = re.split('#', test_box_all_ij)[0]
                if len(deep_learning_df[deep_learning_df["VARIANT_ID"] == test_box_all_ij_id]) > 0:
                    # print(test_box_all_ij_id) 
                    test_box_reference[i, j] = deep_learning_df[deep_learning_df[
                        "VARIANT_ID"] == test_box_all_ij_id].index.values[0]
                    ii.append(i)    
                else:
                    test_box_reference[i, j] = -100
                    missing_items.append(test_box_all_ij_id)

            if (test_box_all.loc[i][j] != "Missing" and j % 2 == 1):
                test_box_all_ij = test_box_all.loc[i][j]
                test_box_all_ij_id = re.split('#', test_box_all_ij)[0]
                # print(test_box_all_ij_id)
                if len(deep_learning_df[deep_learning_df["VARIANT_ID"] == test_box_all_ij_id]) > 0:
                    # print(test_box_all_ij_id)
                    test_box_reference[i, j] = deep_learning_df[deep_learning_df[
                        "VARIANT_ID"] == test_box_all_ij_id].index.values[0]
                else:
                    # print(test_box_all_ij_id)
                    test_box_reference[i, j] = -100
                    missing_items.append(test_box_all_ij_id)

    test_box_reference = pd.DataFrame(test_box_reference, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11',
                                                                   '12', '13', '14', '15', '16', '17', '18', '19', '20', '21',
                                                                   '22', '23', '24', '25', '26', '27', '28', '29', '30', '31',
                                                                   '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', ])
    # '42', '43', '44', '45', '46', '47', '48', '49', '50', '51',
    # '52', '53', '54', '55', '56', '57', '58', '59', '60', '61',
    # '62', '63', '64', '65', '66', '67', '68', '69', '70', '71'])
    test_box_reference = test_box_reference.drop(
        ["3", '5', '7', '9', '11', '13', '15', '17', '19', '21',
         "23", "25", "27", "29", "31", "33", "35", "37", "39", "41", ], axis=1)
    # "43", "45", "47", "49", "51", "53", "55", "57", "59", "61",
    # "63", "65", "67", "69", "71"], axis=1)

    # print(ii)
    # print(test_box_reference.iloc[ii[0]])
    # print(test_box_reference.head(50))

    column_names = list(test_box_reference.columns.values)
    test_box_reference = test_box_reference[
        (test_box_reference[column_names] != -100).all(axis=1)]

    index_value = list(test_box_reference.index)
    # print(index_value)
    csv_path = output_table_path_8
    test_box = pd.read_csv(cwd + csv_path)
    test_box = test_box.loc[index_value]

    print(test_box.head(5))

    test_box.to_csv(cwd + output_table_path_8, index=False)

    test_box_reference.to_csv(cwd + output_table_path_7)

    print(test_box_reference.head(5))

    # csv_path = output_table_path_7
    # test_box_reference = pd.read_csv(cwd + csv_path)
    # test_box_reference.to_csv(cwd + output_table_path_7, index=False)

    # print(len(test_box_reference))
    # print(test_box_reference.head(10))

    # print("136" + " " + deep_learning_df["VARIANT_ID"][136])
    # print("52254" + " " + deep_learning_df["VARIANT_ID"][52254])
    # print("3001" + " " + deep_learning_df["VARIANT_ID"][3001])
    # print("2938" + " " + deep_learning_df["VARIANT_ID"][2938])

    print("len(missing_items) = ", len(missing_items))

    # print(deep_learning_df[deep_learning_df["VARIANT_ID"] == "prod210500492-GRAY"])
    # print(deep_learning_df[deep_learning_df["VARIANT_ID"] == "prod218050114-NAVY"])

    print("precheckbox is done!")

    return num_items
