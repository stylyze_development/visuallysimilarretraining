import csv
import numpy as np
import os
import pandas as pd


def parse_data(output_table_path_5,
               output_table_path_6,
               output_table_path_7,
               output_table_path_8,
               output_table_path_10,
               output_table_path_11,
               num_items
               ):

    # num_items = (search_size_2 - 1) * 2 + 1

    cwd = os.getcwd()

    test_box_reference = pd.read_csv(cwd + output_table_path_7)
    test_box = pd.read_csv(cwd + output_table_path_8)
    print("******************************************************************") 
    print("len(test_box) = ", len(test_box))
    print(test_box.head(5))
    print("len(test_box_reference) = ", len(test_box_reference))
    print(test_box_reference.head(5))

    test_box = test_box.fillna(False)
    test_p = test_box_reference.copy(deep=True)
    test_n = test_box_reference.copy(deep=True)
    length = len(test_p)
    for i in range(length):
        if test_box.loc[i][0] == True:
            for j in range(1, num_items):
                jj = np.floor(j / 2)
                if jj < j / 2:
                    jj = int(jj)
                    if test_box.loc[i][j] == True:
                        test_p.loc[i][
                            jj + 2] = test_box_reference.loc[i][jj + 2]   
                    else:
                        test_p.loc[i][jj + 2] = -1
                if jj == j / 2:
                    jj = int(jj)
                    if test_box.loc[i][j] == True:
                        test_n.loc[i][
                            jj + 1] = test_box_reference.loc[i][jj + 1]
                    else:
                        test_n.loc[i][jj + 1] = -1
        else:
            for j in range(1, num_items):
                jj = np.floor(j / 2)
                if jj < j / 2:
                    jj = int(jj)
                    test_p.loc[i][jj + 2] = -1
                if jj == j / 2:
                    jj = int(jj)
                    test_n.loc[i][jj + 1] = -1

    print("len(test_p) = ", len(test_p)) 
    print("len(test_n) = ", len(test_n))  

    indices = np.random.permutation(len(test_p))              
    test_pp = test_p.loc[indices]
    test_nn = test_n.loc[indices]

    train_size = int(len(test_p)*0.7)
    test_pp_train = test_pp.iloc[:train_size]
    test_nn_train = test_nn.iloc[:train_size]

    test_pp_test = test_pp.iloc[train_size:]
    test_nn_test = test_nn.iloc[train_size:]

    print("len(test_pp_train) = ", len(test_pp_train)) 
    print("len(test_nn_train) = ", len(test_nn_train)) 

    print("len(test_pp_test) = ", len(test_pp_test)) 
    print("len(test_nn_test) = ", len(test_nn_test)) 

    item_all = []
    for i in range(len(test_pp_train)):
        for j in range(1,22):
            item = test_pp_train.iloc[i,j]
            item_2 = test_nn_train.iloc[i,j]
            if item not in item_all and item != -1:
                item_all.append(item)
            if item_2 not in item_all and item_2 != -1:
                item_all.append(item_2)  

    if len(item_all) > len(set(item_all)):
        print("duplicates found!")

    for i in range(len(test_pp_test)):
        for j in range(1,22):
            item = test_pp_test.iloc[i,j]
            item_2 = test_nn_test.iloc[i,j]
            if item in item_all:
                test_pp_test.iloc[i,j] = -1
            if item_2 in item_all:
                test_nn_test.iloc[i,j] = -1  

    # count_posi = 0
    # for i in range(len(test_pp_train)):
    #     if len(test_pp_train[i]) == 0:
    #         count_posi += 0 
    # count_nega = 0        
    # for i in range(len(test_nn_train)):
    #     if len(test_nn_train[i]) == 0:
    #         count_nega += 0  
    # print("count_posi_train = ", count_posi) 
    # print("count_nega_train = ", count_nega) 

    # count_posi = 0
    # for i in range(len(test_pp_test)):
    #     if len(test_pp_test[i]) == 0:
    #         count_posi += 0 
    # count_nega = 0        
    # for i in range(len(test_nn_test)):
    #     if len(test_nn_test[i]) == 0:
    #         count_nega += 0  
    # print("count_posi_test = ", count_posi) 
    # print("count_nega_test = ", count_nega)             

    print("test_pp_test", test_pp_test)
    print("test_nn_test", test_nn_test)            

    test_pp_test = test_pp_test[test_pp_test["1"] != -1] 
    test_nn_test = test_nn_test[test_nn_test["1"] != -1]    

    print("len(test_pp_train) = ", len(test_pp_train)) 
    print("len(test_nn_train) = ", len(test_nn_train)) 

    print("len(test_pp_test) = ", len(test_pp_test)) 
    print("len(test_nn_test) = ", len(test_nn_test))          
                  
    test_pp_train = test_pp_train.drop(columns=["Unnamed: 0"])
    test_nn_train = test_nn_train.drop(columns=["Unnamed: 0"])
    
    test_pp_train.to_csv(cwd + output_table_path_5)
    test_nn_train.to_csv(cwd + output_table_path_6)

    test_pp_test = test_pp_test.drop(columns=["Unnamed: 0"])
    test_nn_test = test_nn_test.drop(columns=["Unnamed: 0"])
    
    test_pp_test.to_csv(cwd + output_table_path_10)
    test_nn_test.to_csv(cwd + output_table_path_11)

    # print("136" + " " + deep_learning_df["VARIANT_ID"][136])
    # print("52254" + " " + deep_learning_df["VARIANT_ID"][52254])
    # print("3001" + " " + deep_learning_df["VARIANT_ID"][3001])
    # print("2938" + " " + deep_learning_df["VARIANT_ID"][2938])
    print("test_pp_train", test_pp_train)
    print("test_nn_train", test_nn_train)
    print("test_pp_test", test_pp_test)
    print("test_nn_test", test_nn_test)

    print("checkbox is done!")

