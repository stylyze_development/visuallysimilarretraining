import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf
from matplotlib import pyplot as plt

from . import traingenerator
from . import loss
from . import modelsre as models


# logging.basicConfig(level=logging.INFO)


def embedding(model_input_path_2,
              n_H0, n_W0, n_C0,
              batch_size, training_ratio, embedding_size,
              image_normalization,
              same_image_epsilon,
              model_weight_path,
              model_weight_path_2,
              model_name,
              output_layer,
              num_epoch,
              MARGIN,
              net_3_dim,
              learning_rate,
              keepratio,
              train_size,
              test_size,
              model_input_path):
    """

    """

    cwd = os.getcwd()

    with open(cwd + model_input_path_2, "rb") as my_file2:
        df_triplet_final = pickle.load(my_file2)

    # df_triplet_final = df_triplet_final.head(960)
    # df_triplet_final = df_triplet_final.head(3200)
    # df_triplet_final = df_triplet_final.head(6400)  
    # df_triplet_final = df_triplet_final.head(12800) 
    # df_triplet_final = df_triplet_final.head(30400)     

    # df_triplet_final = df_triplet_final[50:]
    print("*******************************************")
    print("***TOTAL NUMBER OF TRIPLETS:" + str(len(df_triplet_final)))

    grouped = df_triplet_final

    num_epoch = 60
    batch_size = 32
    training_ratio = 0.9
    MARGIN = 1
    n_H0, n_W0, n_C0 = (224, 224, 3)
    net_3_dim = 4096
    learning_rate = 0.001
    keepratio = 0.5

    # Parameters
    params = {'dim_x': n_W0,
              'dim_y': n_H0,
              'dim_z': n_C0,
              'batch_size': batch_size,
              'n_y': 1,
              'shuffle': False}

    # Start the tensorflow process
    init = tf.global_variables_initializer()
    with tf.Session() as sess:

        sess.run(init)

        new_saver = tf.train.import_meta_graph(
            cwd + "/weights/" + 'my_test_model.meta')
        new_saver.restore(
            sess, tf.train.latest_checkpoint(cwd + "/weights/"))
        graph = tf.get_default_graph()

        # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]

        # print(tensors)

        wd1 = graph.get_tensor_by_name("wd1:0")
        wd2 = graph.get_tensor_by_name("wd2:0")
        bd1 = graph.get_tensor_by_name("bd1:0")
        bd2 = graph.get_tensor_by_name("bd2:0")

        # print(wd1)

        # conv1_1 = graph.get_tensor_by_name(
        #     "conv1_1:0")
        # conv1_2 = graph.get_tensor_by_name(
        #     "conv1_2:0")
        # conv2_1 = graph.get_tensor_by_name(
        #     "conv2_1:0")
        # conv2_2 = graph.get_tensor_by_name(
        #     "conv2_2:0")
        # conv3_1 = graph.get_tensor_by_name(
        #     "conv3_1:0")
        # conv3_2 = graph.get_tensor_by_name(
        #     "conv3_2:0")
        # conv3_3 = graph.get_tensor_by_name(
        #     "conv3_3:0")
        # conv3_4 = graph.get_tensor_by_name(
        #     "conv3_4:0")
        # conv4_1 = graph.get_tensor_by_name(
        #     "conv4_1:0")
        # conv4_2 = graph.get_tensor_by_name(
        #     "conv4_2:0")
        # conv4_3 = graph.get_tensor_by_name(
        #     "conv4_3:0")
        # conv4_4 = graph.get_tensor_by_name(
        #     "conv4_4:0")
        # conv5_1 = graph.get_tensor_by_name(
        #     "conv5_1:0")
        # conv5_2 = graph.get_tensor_by_name(
        #     "conv5_2:0")
        # conv5_3 = graph.get_tensor_by_name(
        #     "conv5_3:0")
        # conv5_4 = graph.get_tensor_by_name(
        #     "conv5_4:0")

        # bias1_1 = graph.get_tensor_by_name(
        #     "bias1_1:0")
        # bias1_2 = graph.get_tensor_by_name(
        #     "bias1_2:0")
        # bias2_1 = graph.get_tensor_by_name(
        #     "bias2_1:0")
        # bias2_2 = graph.get_tensor_by_name(
        #     "bias2_2:0")
        # bias3_1 = graph.get_tensor_by_name(
        #     "bias3_1:0")
        # bias3_2 = graph.get_tensor_by_name(
        #     "bias3_2:0")
        # bias3_3 = graph.get_tensor_by_name(
        #     "bias3_3:0")
        # bias3_4 = graph.get_tensor_by_name(
        #     "bias3_4:0")
        # bias4_1 = graph.get_tensor_by_name(
        #     "bias4_1:0")
        # bias4_2 = graph.get_tensor_by_name(
        #     "bias4_2:0")
        # bias4_3 = graph.get_tensor_by_name(
        #     "bias4_3:0")
        # bias4_4 = graph.get_tensor_by_name(
        #     "bias4_4:0")
        # bias5_1 = graph.get_tensor_by_name(
        #     "bias5_1:0")
        # bias5_2 = graph.get_tensor_by_name(
        #     "bias5_2:0")
        # bias5_3 = graph.get_tensor_by_name(
        #     "bias5_3:0")
        # bias5_4 = graph.get_tensor_by_name(
        #     "bias5_4:0")
          
        w1 = sess.run(wd1)
        w2 = sess.run(wd2)
        b1 = sess.run(bd1)
        b2 = sess.run(bd2)

    tf.reset_default_graph() 

    with tf.variable_scope("layer", reuse=tf.AUTO_REUSE):                

        img_placeholder_a = tf.placeholder(tf.float32, shape=(
            None, n_H0, n_W0, n_C0), name="img_placeholder_a")
        img_placeholder_p = tf.placeholder(tf.float32, shape=(
            None, n_H0, n_W0, n_C0), name="img_placeholder_p")
        img_placeholder_n = tf.placeholder(tf.float32, shape=(
            None, n_H0, n_W0, n_C0), name="img_placeholder_n")

        keepratio_tf = tf.placeholder(tf.float32, name="keepratio_tf")

        # Construct the new model
        model_weight_path_update = cwd + model_weight_path

        net_val_a, mean_pixel_a = models.vgg19_no_top_trainable(
            model_weight_path_update, img_placeholder_a)
        net_val_p, mean_pixel_p = models.vgg19_no_top_trainable(
            model_weight_path_update, img_placeholder_p)
        net_val_n, mean_pixel_n = models.vgg19_no_top_trainable(
            model_weight_path_update, img_placeholder_n)

        train_features_a = net_val_a['pool5']
        train_features_aa = tf.contrib.layers.flatten(train_features_a)
        train_features_p = net_val_p['pool5']
        train_features_pp = tf.contrib.layers.flatten(train_features_p)
        train_features_n = net_val_n['pool5']
        train_features_nn = tf.contrib.layers.flatten(train_features_n)

        # # def initialize_parameters():
        # # # tf.set_random_seed(1)
        # conv1_1 = tf.get_variable(
        #     "conv1_1", [3, 3, 3, 64], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv1_2 = tf.get_variable(
        #     "conv1_2", [3, 3, 64, 64], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # conv2_1 = tf.get_variable(
        #     "conv2_1", [3, 3, 64, 128], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv2_2 = tf.get_variable(
        #     "conv2_2", [3, 3, 128, 128], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # conv3_1 = tf.get_variable(
        #     "conv3_1", [3, 3, 128, 256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv3_2 = tf.get_variable(
        #     "conv3_2", [3, 3, 256, 256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv3_3 = tf.get_variable(
        #     "conv3_3", [3, 3, 256, 256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv3_4 = tf.get_variable(
        #     "conv3_4", [3, 3, 256, 256], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # conv4_1 = tf.get_variable(
        #     "conv4_1", [3, 3, 256, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv4_2 = tf.get_variable(
        #     "conv4_2", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv4_3 = tf.get_variable(
        #     "conv4_3", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv4_4 = tf.get_variable(
        #     "conv4_4", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # conv5_1 = tf.get_variable(
        #     "conv5_1", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv5_2 = tf.get_variable(
        #     "conv5_2", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv5_3 = tf.get_variable(
        #     "conv5_3", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # conv5_4 = tf.get_variable(
        #     "conv5_4", [3, 3, 512, 512], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # bias1_1 = tf.get_variable(
        #     "bias1_1", [64], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias1_2 = tf.get_variable(
        #     "bias1_2", [64], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # bias2_1 = tf.get_variable(
        #     "bias2_1", [128], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias2_2 = tf.get_variable(
        #     "bias2_2", [128], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # bias3_1 = tf.get_variable(
        #     "bias3_1", [256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias3_2 = tf.get_variable(
        #     "bias3_2", [256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias3_3 = tf.get_variable(
        #     "bias3_3", [256], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias3_4 = tf.get_variable(
        #     "bias3_4", [256], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # bias4_1 = tf.get_variable(
        #     "bias4_1", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias4_2 = tf.get_variable(
        #     "bias4_2", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias4_3 = tf.get_variable(
        #     "bias4_3", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias4_4 = tf.get_variable(
        #     "bias4_4", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # bias5_1 = tf.get_variable(
        #     "bias5_1", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias5_2 = tf.get_variable(
        #     "bias5_2", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias5_3 = tf.get_variable(
        #     "bias5_3", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))
        # bias5_4 = tf.get_variable(
        #     "bias5_4", [512], initializer=tf.contrib.layers.xavier_initializer(seed=0))

        # # parameters = {"conv1_1": conv1_1, "conv1_2": conv1_2,
        # #               "conv2_1": conv2_1, "conv2_2": conv2_2,
        # #               "conv3_1": conv3_1, "conv3_2": conv3_2, "conv3_3": conv3_3, "conv3_4": conv3_4
        # #               "conv4_1": conv4_1, "conv4_2": conv4_2, "conv4_3": conv4_3, "conv4_4": conv4_4
        # #               "conv5_1": conv5_1, "conv5_2": conv5_2, "conv5_3": conv5_3, "conv5_4": conv5_4}
        # # return parameters

        # def conv_layer(input, weight, bias):
        #     # CONV2D: stride of 1, padding 'SAME'
        #     conv = tf.nn.conv2d(input, weight, strides=(1, 1, 1, 1), padding='SAME')
        #     return tf.nn.relu(tf.nn.bias_add(conv, bias))

        # def pool_layer(input):
        #     # MAXPOOL: window 2x2, sride 2, padding 'SAME'
        #     return tf.nn.max_pool(input, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1), padding='SAME')

        # def _drop_out_layer(input):
        #     # DROP-OUT
        #     return tf.nn.dropout(input, 0.5)

        # conv1_1_out_a = conv_layer(img_placeholder_a, conv1_1, bias1_1)
        # conv1_2_out_a = conv_layer(conv1_1_out_a, conv1_2, bias1_2)
        # pool1_out_a = pool_layer(conv1_2_out_a)

        # conv2_1_out_a = conv_layer(pool1_out_a, conv2_1, bias2_1)
        # conv2_2_out_a = conv_layer(conv2_1_out_a, conv2_2, bias2_2)
        # pool2_out_a = pool_layer(conv2_2_out_a)

        # conv3_1_out_a = conv_layer(pool2_out_a, conv3_1, bias3_1)
        # conv3_2_out_a = conv_layer(conv3_1_out_a, conv3_2, bias3_2)
        # conv3_3_out_a = conv_layer(conv3_2_out_a, conv3_3, bias3_3)
        # conv3_4_out_a = conv_layer(conv3_3_out_a, conv3_4, bias3_4)
        # pool3_out_a = pool_layer(conv3_4_out_a)

        # conv4_1_out_a = conv_layer(pool3_out_a, conv4_1, bias4_1)
        # conv4_2_out_a = conv_layer(conv4_1_out_a, conv4_2, bias4_2)
        # conv4_3_out_a = conv_layer(conv4_2_out_a, conv4_3, bias4_3)
        # conv4_4_out_a = conv_layer(conv4_3_out_a, conv4_4, bias4_4)
        # pool4_out_a = pool_layer(conv4_4_out_a)

        # conv5_1_out_a = conv_layer(pool4_out_a, conv5_1, bias5_1)
        # conv5_2_out_a = conv_layer(conv5_1_out_a, conv5_2, bias5_2)
        # conv5_3_out_a = conv_layer(conv5_2_out_a, conv5_3, bias5_3)
        # conv5_4_out_a = conv_layer(conv5_3_out_a, conv5_4, bias5_4)
        # pool5_out_a = pool_layer(conv5_4_out_a) 

        # conv1_1_out_p = conv_layer(img_placeholder_p, conv1_1, bias1_1)
        # conv1_2_out_p = conv_layer(conv1_1_out_p, conv1_2, bias1_2)
        # pool1_out_p = pool_layer(conv1_2_out_p)

        # conv2_1_out_p = conv_layer(pool1_out_p, conv2_1, bias2_1)
        # conv2_2_out_p = conv_layer(conv2_1_out_p, conv2_2, bias2_2)
        # pool2_out_p = pool_layer(conv2_2_out_p)

        # conv3_1_out_p = conv_layer(pool2_out_p, conv3_1, bias3_1)
        # conv3_2_out_p = conv_layer(conv3_1_out_p, conv3_2, bias3_2)
        # conv3_3_out_p = conv_layer(conv3_2_out_p, conv3_3, bias3_3)
        # conv3_4_out_p = conv_layer(conv3_3_out_p, conv3_4, bias3_4)
        # pool3_out_p = pool_layer(conv3_4_out_p)

        # conv4_1_out_p = conv_layer(pool3_out_p, conv4_1, bias4_1)
        # conv4_2_out_p = conv_layer(conv4_1_out_p, conv4_2, bias4_2)
        # conv4_3_out_p = conv_layer(conv4_2_out_p, conv4_3, bias4_3)
        # conv4_4_out_p = conv_layer(conv4_3_out_p, conv4_4, bias4_4)
        # pool4_out_p = pool_layer(conv4_4_out_p)

        # conv5_1_out_p = conv_layer(pool4_out_p, conv5_1, bias5_1)
        # conv5_2_out_p = conv_layer(conv5_1_out_p, conv5_2, bias5_2)
        # conv5_3_out_p = conv_layer(conv5_2_out_p, conv5_3, bias5_3)
        # conv5_4_out_p = conv_layer(conv5_3_out_p, conv5_4, bias5_4)
        # pool5_out_p = pool_layer(conv5_4_out_p)    

        # conv1_1_out_n = conv_layer(img_placeholder_n, conv1_1, bias1_1)
        # conv1_2_out_n = conv_layer(conv1_1_out_n, conv1_2, bias1_2)
        # pool1_out_n = pool_layer(conv1_2_out_n)

        # conv2_1_out_n = conv_layer(pool1_out_n, conv2_1, bias2_1)
        # conv2_2_out_n = conv_layer(conv2_1_out_n, conv2_2, bias2_2)
        # pool2_out_n = pool_layer(conv2_2_out_n)

        # conv3_1_out_n = conv_layer(pool2_out_n, conv3_1, bias3_1)
        # conv3_2_out_n = conv_layer(conv3_1_out_n, conv3_2, bias3_2)
        # conv3_3_out_n = conv_layer(conv3_2_out_n, conv3_3, bias3_3)
        # conv3_4_out_n = conv_layer(conv3_3_out_n, conv3_4, bias3_4)
        # pool3_out_n = pool_layer(conv3_4_out_n)

        # conv4_1_out_n = conv_layer(pool3_out_n, conv4_1, bias4_1)
        # conv4_2_out_n = conv_layer(conv4_1_out_n, conv4_2, bias4_2)
        # conv4_3_out_n = conv_layer(conv4_2_out_n, conv4_3, bias4_3)
        # conv4_4_out_n = conv_layer(conv4_3_out_n, conv4_4, bias4_4)
        # pool4_out_n = pool_layer(conv4_4_out_n)

        # conv5_1_out_n = conv_layer(pool4_out_n, conv5_1, bias5_1)
        # conv5_2_out_n = conv_layer(conv5_1_out_n, conv5_2, bias5_2)
        # conv5_3_out_n = conv_layer(conv5_2_out_n, conv5_3, bias5_3)
        # conv5_4_out_n = conv_layer(conv5_3_out_n, conv5_4, bias5_4)
        # pool5_out_n = pool_layer(conv5_4_out_n)       

        weights = {
            'wd1': tf.Variable(tf.constant(w1), name="wd1"),
            'wd2': tf.Variable(tf.constant(w2), name="wd2")
        }
        biases = {
            'bd1': tf.Variable(tf.constant(b1), name="bd1"),
            'bd2': tf.Variable(tf.constant(b2), name="bd2")
        }

        def conv_basic(_input, _w, _b, keepratio_tf):
            # Input
            _input_r = _input
            # Vectorize
            _dense1 = tf.reshape(
                _input_r, [-1, _w['wd1'].get_shape().as_list()[0]])
            # Fc1
            _fc1 = tf.nn.relu(tf.add(tf.matmul(_dense1, _w['wd1']), _b['bd1']))
            _fc_dr1 = tf.nn.dropout(_fc1, keepratio_tf)
            # _out = tf.nn.dropout(_fc1, keepratio_tf)
            # Fc2
            _out = tf.nn.relu(tf.add(tf.matmul(_fc_dr1, _w['wd2']), _b['bd2']))
            # out = {'input_r': _input_r, 'dense1': _dense1,
            # 'fc1': _fc1, 'out': _out}
            out = {'input_r': _input_r, 'dense1': _dense1,
                   'fc1': _fc1, 'fc_dr1': _fc_dr1, 'out': _out}
            return out

        # train_features_aa = pool5_out_a
        # train_features_pp = pool5_out_p
        # train_features_nn = pool5_out_n

        train_features_aaa = conv_basic(train_features_aa, weights, biases, keepratio_tf)['out']
        train_features_ppp = conv_basic(train_features_pp, weights, biases, keepratio_tf)['out']
        train_features_nnn = conv_basic(train_features_nn, weights, biases, keepratio_tf)['out']

        # train_features_a = net_val_a['relu7']
        # train_features_aa = tf.contrib.layers.flatten(train_features_a)
        # train_features_p = net_val_p['relu7']
        # train_features_pp = tf.contrib.layers.flatten(train_features_p)
        # train_features_n = net_val_n['relu7']
        # train_features_nn = tf.contrib.layers.flatten(train_features_n)

        # train_features_a = net_val_a['relu5_4']
        # train_features_aa = tf.contrib.layers.flatten(train_features_a)
        # train_features_p = net_val_p['relu5_4']
        # train_features_pp = tf.contrib.layers.flatten(train_features_p)
        # train_features_n = net_val_n['relu5_4']
        # train_features_nn = tf.contrib.layers.flatten(train_features_n)

        # train_features_aa_1 = conv_basic(train_features_aa, weights, biases, keepratio)['out']
        # train_features_pp_1 = conv_basic(train_features_pp, weights, biases, keepratio)['out']
        # train_features_nn_1 = conv_basic(train_features_nn, weights, biases, keepratio)['out']

        # train_features_aa_2 = tf.nn.l2_normalize(train_features_aa, dim=1)
        # train_features_pp_2 = tf.nn.l2_normalize(train_features_pp, dim=1)
        # train_features_nn_2 = tf.nn.l2_normalize(train_features_nn, dim=1)

        # parameters = models.initialize_parameters()
        # Z3_a = models.forward_propagation(
        #     img_placeholder_a, parameters, keepratio_tf, net_3_dim)
        # Z3_p = models.forward_propagation_2(
        #     img_placeholder_p, parameters, keepratio_tf, net_3_dim)
        # Z3_n = models.forward_propagation_2(
        #     img_placeholder_n, parameters, keepratio_tf, net_3_dim)

        # Z3_a_2 = tf.nn.l2_normalize(Z3_a, dim=1)
        # Z3_p_2 = tf.nn.l2_normalize(Z3_p, dim=1)
        # Z3_n_2 = tf.nn.l2_normalize(Z3_n, dim=1)

        # embedding_in_a = tf.concat([train_features_aa_2, Z3_a_2], 1)
        # # embedding_out_a = Z3_a
        # embedding_out_a = models.linear_encoding(embedding_in_a, net_3_dim)

        # embedding_in_p = tf.concat([train_features_pp_2, Z3_p_2], 1)
        # # embedding_out_p = Z3_p
        # embedding_out_p = models.linear_encoding_2(embedding_in_p, net_3_dim)

        # embedding_in_n = tf.concat([train_features_nn_2, Z3_n_2], 1)
        # # embedding_out_n = Z3_n
        # embedding_out_n = models.linear_encoding_2(embedding_in_n, net_3_dim)

        # train_features_aaa = embedding_out_a
        # train_features_ppp = embedding_out_p
        # train_features_nnn = embedding_out_n

        train_features_aaa = tf.nn.l2_normalize(
            train_features_aaa, dim=1, name="train_features_aaa")
        train_features_ppp = tf.nn.l2_normalize(
            train_features_ppp, dim=1, name="train_features_ppp")
        train_features_nnn = tf.nn.l2_normalize(
            train_features_nnn, dim=1, name="train_features_nnn")

    graph = tf.get_default_graph()
    variables = graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    print(variables)

    variables2 = graph.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    print(variables2)

    variables3 = graph.get_collection(tf.GraphKeys.MODEL_VARIABLES)
    print(variables3)

    variables4 = graph.get_collection(tf.GraphKeys.LOCAL_VARIABLES)
    print(variables4)

    variables5 = graph.get_collection(tf.GraphKeys.SUMMARIES)
    print(variables5)

    # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]
    # print(tensors)

    # Define the cost function
    cost, positives, negatives = loss.compute_triplet_loss(
        train_features_aaa, train_features_ppp, train_features_nnn, MARGIN)

    # Define the optimizer
    optm = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    total_bad_imgs_a = 0
    total_bad_imgs_p = 0
    total_bad_imgs_n = 0
    total_bad_imgs = 0

    total_a = 0
    total_p = 0
    total_n = 0

    total_cost = 0
    group_index = 0

    deep_learning_dic = {}

    group_i = grouped

    index_a = group_i["ANCHOR"].tolist()
    # print("************************************************************************************")
    # print(index_a[:100])
    # print(index_a[-100:])
    # print("************************************************************************************")
    image_path_a = group_i["ANCHOR_CURATOR_IMAGE_PATH"]
    image_path_a.index = index_a

    index_p = group_i["POSITIVE"].tolist()
    image_path_p = group_i["POSITIVE_CURATOR_IMAGE_PATH"]
    image_path_p.index = index_p

    index_n = group_i["NEGATIVE"].tolist()
    image_path_n = group_i["NEGATIVE_CURATOR_IMAGE_PATH"]
    image_path_n.index = index_n

    y_labels_a = np.zeros((len(index_a), 1))
    # print("***NUMBER OF SAMPLES:" + str(len(y_labels_a)))
    total_a += len(y_labels_a)
    # print("***TOTAL NUMBER OF SAMPLES:" + str(total_a))
    y_labels_p = np.zeros((len(index_p), 1))
    # print("***NUMBER OF SAMPLES:" + str(len(y_labels_p)))
    total_p += len(y_labels_p)
    # print("***TOTAL NUMBER OF SAMPLES:" + str(total_p))
    y_labels_n = np.zeros((len(index_n), 1))
    # print("***NUMBER OF SAMPLES:" + str(len(y_labels_n)))
    total_n += len(y_labels_n)
    # print("***TOTAL NUMBER OF SAMPLES:" + str(total_n))

    number_of_products = len(y_labels_a)
    m = number_of_products

    y_labels_training_a = {}
    partition_training_a = []
    for i in range(train_size):
    # for i in range(0, int(number_of_products * training_ratio)):
        index_i_a = int(index_a[i])
        y_labels_training_a[str(index_i_a)] = y_labels_a[i, :]
        partition_training_a.append(str(index_i_a))

    y_labels_training_p = {}
    partition_training_p = []
    for i in range(train_size):
    # for i in range(0, int(number_of_products * training_ratio)):
        index_i_p = int(index_p[i])
        y_labels_training_p[str(index_i_p)] = y_labels_p[i, :]
        partition_training_p.append(str(index_i_p))

    y_labels_training_n = {}
    partition_training_n = []
    for i in range(train_size):
    # for i in range(0, int(number_of_products * training_ratio)):
        index_i_n = int(index_n[i])
        y_labels_training_n[str(index_i_n)] = y_labels_n[i, :]
        partition_training_n.append(str(index_i_n))

    y_labels_testing_a = {}
    partition_testing_a = []
    for i in range(len(df_triplet_final)-test_size,len(df_triplet_final)):
    # for i in range(int(number_of_products * training_ratio), number_of_products):
        index_i_a = int(index_a[i])
        y_labels_testing_a[str(index_i_a)] = y_labels_a[i, :]
        partition_testing_a.append(str(index_i_a))

    y_labels_testing_p = {}
    partition_testing_p = []
    for i in range(len(df_triplet_final)-test_size,len(df_triplet_final)):
    # for i in range(int(number_of_products * training_ratio), number_of_products):
        index_i_p = int(index_p[i])
        y_labels_testing_p[str(index_i_p)] = y_labels_p[i, :]
        partition_testing_p.append(str(index_i_p))

    y_labels_testing_n = {}
    partition_testing_n = []
    for i in range(len(df_triplet_final)-test_size,len(df_triplet_final)):
    # for i in range(int(number_of_products * training_ratio), number_of_products):
        index_i_n = int(index_n[i])
        y_labels_testing_n[str(index_i_n)] = y_labels_n[i, :]
        partition_testing_n.append(str(index_i_n))

    training_generator_a = traingenerator.DataGenerator(
        **params).generate(y_labels_training_a, partition_training_a, image_path_a)
    training_generator_p = traingenerator.DataGenerator(
        **params).generate(y_labels_training_p, partition_training_p, image_path_p)
    training_generator_n = traingenerator.DataGenerator(
        **params).generate(y_labels_training_n, partition_training_n, image_path_n)

    testing_generator_a = traingenerator.DataGenerator(
        **params).generate(y_labels_testing_a, partition_testing_a, image_path_a)
    testing_generator_p = traingenerator.DataGenerator(
        **params).generate(y_labels_testing_p, partition_testing_p, image_path_p)
    testing_generator_n = traingenerator.DataGenerator(
        **params).generate(y_labels_testing_n, partition_testing_n, image_path_n)

    minibatch_size = batch_size
    num_minibatches_training =  int(train_size/32)
    # num_minibatches_training = np.int(
    #     np.round(m * training_ratio) / minibatch_size)
    minibatches_training_a = training_generator_a
    minibatches_training_p = training_generator_p
    minibatches_training_n = training_generator_n

    num_minibatches_testing = int(test_size/32)
    # num_minibatches_testing = np.int(
    #     np.round(m * (1 - training_ratio)) / minibatch_size)
    minibatches_testing_a = testing_generator_a
    minibatches_testing_p = testing_generator_p
    minibatches_testing_n = testing_generator_n

    print("TRAINING BATCH SIZE: " + str(num_minibatches_training))
    print("TESTING BATCH SIZE: " + str(num_minibatches_testing))

    minibatch_cost_np = []
    minibatch_cost_testing_np = []

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        # values = [sess.run(v) for v in variables]
        # print(values)

        print("*******************************************")
        print("***TRAINING BEGINS...")
        for epoch in range(num_epoch):
            if len(group_i) >= 1:

                minibatch_cost = 0
                minibatch_positives = 0
                minibatch_negatives = 0
                startTime = datetime.now()
                # matrix_res_aaa = np.zeros(
                #     [np.int(np.round(m * training_ratio)), net_3_dim])
                # matrix_res_ppp = np.zeros(
                #     [np.int(np.round(m * training_ratio)), net_3_dim])
                # matrix_res_nnn = np.zeros(
                #     [np.int(np.round(m * training_ratio)), net_3_dim])
                for i in range(num_minibatches_training):
                    (minibatch_X_training_a, minibatch_Y_training_a,
                     bad_img_a, ID_a) = next(minibatches_training_a)
                    (minibatch_X_training_p, minibatch_Y_training_p,
                     bad_img_p, ID_p) = next(minibatches_training_p)
                    (minibatch_X_training_n, minibatch_Y_training_n,
                     bad_img_n, ID_n) = next(minibatches_training_n)

                    total_bad_imgs_a += bad_img_a
                    total_bad_imgs_p += bad_img_p
                    total_bad_imgs_n += bad_img_n
                    total_bad_imgs += total_bad_imgs_a + total_bad_imgs_p + total_bad_imgs_n

                    sess.run(optm, feed_dict={img_placeholder_a: minibatch_X_training_a,
                                              img_placeholder_p: minibatch_X_training_p,
                                              img_placeholder_n: minibatch_X_training_n,
                                              keepratio_tf: keepratio})

                    minibatch_cost += sess.run(cost, feed_dict={img_placeholder_a: minibatch_X_training_a,
                                                                img_placeholder_p: minibatch_X_training_p,
                                                                img_placeholder_n: minibatch_X_training_n,
                                                                keepratio_tf: 1.0}) / num_minibatches_training

                    # minibatch_positives += sess.run(positives, feed_dict={img_placeholder_a: minibatch_X_training_a,
                    #                                                       img_placeholder_p: minibatch_X_training_p,
                    # img_placeholder_n: minibatch_X_training_n}) /
                    # num_minibatches_training

                    # minibatch_negatives += sess.run(negatives, feed_dict={img_placeholder_a: minibatch_X_training_a,
                    #                                                        img_placeholder_p: minibatch_X_training_p,
                    # img_placeholder_n: minibatch_X_training_n}) /
                    # num_minibatches_training

                    # total_cost += minibatch_cost

                    # train_features_aaa_np = train_features_aaa.eval(
                    #     feed_dict={img_placeholder_a: minibatch_X_training_a, keepratio_tf: keepratio})
                    # train_features_ppp_np = train_features_ppp.eval(
                    #     feed_dict={img_placeholder_p: minibatch_X_training_p, keepratio_tf: keepratio})
                    # train_features_nnn_np = train_features_nnn.eval(
                    # feed_dict={img_placeholder_n: minibatch_X_training_n,
                    # keepratio_tf: keepratio})

                    # train_features_aa_2_np = train_features_aa_2.eval(
                    # feed_dict={img_placeholder_a: minibatch_X_training_a})

                    # print(train_features_aaa_np)
                    # print(train_features_ppp_np)
                    # print(train_features_nnn_np)
                    # print("*******************************************")
                    # print("***Batch:" + " " + str(i + 1) + "/" + str(num_minibatches_training) + ";" + " " + "Epoch:" + " " + str(epoch + 1) + "/" + str(num_epoch))

                    if i == num_minibatches_training - 1:
                        print("*******************************************")
                        print("***TRAINING COST:" + " " + str(minibatch_cost))
                        # print("Positive:" + " " + str(minibatch_positives))
                        # print("Negative:" + " " + str(minibatch_negatives))

                        minibatch_cost_np.append(minibatch_cost)

                        # time_used = datetime.now() - startTime
                        # print("***TIME TAKEN:" + str(time_used))
                        # print("***TOTAL NUMBER OF BAD IMAGES:" +
                        #       str(total_bad_imgs))

                    # ntrain = batch_size
                    # train_vectorized_aaa = np.ndarray((ntrain, net_3_dim))
                    # train_vectorized_ppp = np.ndarray((ntrain, net_3_dim))
                    # train_vectorized_nnn = np.ndarray((ntrain, net_3_dim))

                    # for j in range(ntrain):
                    #     curr_feat_aaa = train_features_aaa_np[j, :]
                    #     curr_feat_vec_aaa = np.reshape(
                    #         curr_feat_aaa, (1, -1))
                    #     train_vectorized_aaa[j, :] = curr_feat_vec_aaa

                    #     curr_feat_ppp = train_features_ppp_np[j, :]
                    #     curr_feat_vec_ppp = np.reshape(
                    #         curr_feat_ppp, (1, -1))
                    #     train_vectorized_ppp[j, :] = curr_feat_vec_ppp

                    #     curr_feat_nnn = train_features_nnn_np[j, :]
                    #     curr_feat_vec_nnn = np.reshape(
                    #         curr_feat_nnn, (1, -1))
                    #     train_vectorized_nnn[j, :] = curr_feat_vec_nnn

                    # matrix_res_aaa[
                    #     i * batch_size:(i + 1) * batch_size, :] = train_vectorized_aaa
                    # matrix_res_ppp[
                    #     i * batch_size:(i + 1) * batch_size, :] = train_vectorized_ppp
                    # matrix_res_nnn[
                    # i * batch_size:(i + 1) * batch_size, :] =
                    # train_vectorized_nnn

                # print(np.sum(matrix_res_aaa[700]))
                # print(np.sum(matrix_res_ppp[700]))
                # print(np.sum(matrix_res_nnn[700]))
                # print("*******************************************")

                minibatch_cost_testing = 0
                minibatch_positives_testing = 0
                minibatch_negatives_testing = 0
                # startTime = datetime.now()
                for i in range(num_minibatches_testing):
                    (minibatch_X_testing_a, minibatch_Y_testing_a,
                     bad_img_a, ID_a) = next(minibatches_testing_a)
                    (minibatch_X_testing_p, minibatch_Y_testing_p,
                     bad_img_p, ID_p) = next(minibatches_testing_p)
                    (minibatch_X_testing_n, minibatch_Y_testing_n,
                     bad_img_n, ID_n) = next(minibatches_testing_n)

                    total_bad_imgs_a += bad_img_a
                    total_bad_imgs_p += bad_img_p
                    total_bad_imgs_n += bad_img_n
                    total_bad_imgs += total_bad_imgs_a + total_bad_imgs_p + total_bad_imgs_n

                    minibatch_cost_testing += sess.run(cost, feed_dict={img_placeholder_a: minibatch_X_testing_a,
                                                                        img_placeholder_p: minibatch_X_testing_p,
                                                                        img_placeholder_n: minibatch_X_testing_n,
                                                                        keepratio_tf: 1.0}) / num_minibatches_testing

                    if i == num_minibatches_testing - 1:
                        # print("*******************************************")
                        print("***TESTING COST:" + " " +
                              str(minibatch_cost_testing))

                        minibatch_cost_testing_np.append(
                            minibatch_cost_testing)

                        time_used = datetime.now() - startTime
                        print("***TIME TAKEN:" + str(time_used))
                        print("***TOTAL NUMBER OF BAD IMAGES:" +
                              str(total_bad_imgs))

                print("***EPOCH:" + " " + str(epoch + 1) + " " + "IS FINISHED" +
                      ";" + " " + "TOTAL # OF EPOCH:" + " " + str(num_epoch))

        # # plot the cost
        # plt.switch_backend('agg')
        # fig, ax = plt.subplots()
        # x_epoch = np.arange(num_epoch)
        # plt.plot(x_epoch, minibatch_cost_np)
        # plt.plot(x_epoch, minibatch_cost_testing_np)
        # plt.ylabel('COST')
        # plt.xlabel('EPOCH')
        # # plt.title("Learning rate =" + str(learning_rate))
        # plt.show(fig)
        # fig.savefig(cwd + "/output_tables/" + "test_fig.pdf")

        saver = tf.train.Saver()
        saver.save(sess, cwd + model_weight_path_2 + 'my_test_model')
