import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)
    # df = pd.read_csv(cwd + "/data_tables/df_train.csv")

    # print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES"
                            })

    # Pull the columns needed from the big table
    # df = df[["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "ARCHIVED"]]
    df = df[["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES"]]
    df = df.fillna("Missing")
    # df = df[df["ARCHIVED"] != 1]

    # The following functions are used to parse the type and style attributes
    def parser(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_2(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            # a = a[0]
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    # Apply the parsers to the types and styles
    # df["TYPES"] = df.apply(lambda row: parser(row["TYPES"]), axis=1)
    # df["STYLES"] = df.apply(lambda row: parser_2(row["STYLES"]), axis=1)
    df["CATEGORIES"] = df.apply(lambda row: parser_2(row["CATEGORIES"]), axis=1)

    df_big = df
    # df_big["VARIANT_ID"] = df_big["VARIANT_ID"].astype('int64')

    # Drop the items with missing and/or uncategorized type and/or style
    # df_big = df_big[df_big["STYLES"] != "Missing"]
    # df_big = df_big[df_big["TYPES"] != "Missing"]
    df_big = df_big[df_big["CATEGORIES"] != "Missing"]
    # df_big = df_big[df_big["STYLES"] != "Uncategorized"]
    # df_big = df_big[df_big["TYPES"] != "uncategorized"]

    # df_big = df_big[df_big["CATEGORIES"] == "Earrings"]
    df_big = df_big[df_big["CATEGORIES"] == "Necklaces"]
    # df_big = df_big[df_big["CATEGORIES"] == "Bracelets"]
    # df_big = df_big[df_big["CATEGORIES"] == "Rings"] 
    # df_big = df_big[(df_big["CATEGORIES"] == "Tops") | (df_big["CATEGORIES"] == "Blouses") | (df_big["CATEGORIES"] == "Dresses") |
    # df_big = df_big[(df_big["CATEGORIES"] == "Earrings") | (df_big["CATEGORIES"] == "Necklaces") |
    #   (df_big["CATEGORIES"] == "Bracelets") | (df_big["CATEGORIES"] == "Rings") ]

    # Tops 112
    # 16821
    # Blouses 50
    # 10153
    # Dresses 7
    # 9882
    # Earrings 287
    # 8924
    # Bags 115
    # 8774
    # Pants 
    # 7925
    # Jackets
    # 7693
    # Necklaces 1198
    # 7346
    # Sweaters
    # 7179
    # Heels
    # 6529
    # Bracelets
    # 5957
    # Skirts
    # 5913
    # gowns
    # 5804
    # Jeans
    # 5197
    # Swimwear
    # 5013
    # Dresses,Work Dresses
    # 4723
    # Sunglasses
    # 4105
    # Rings
    # 3953
    # Sneakers
    # 3931
    # Sandals
    # 2748
    # Casual Dresses,Dresses
    # 2641
    # Booties
    # 2602
    # Makeup
    # 2346
    # Blazers
    # 2220
    
    # jewelry
    # 14230
    # tops
    # 7986
    # dresses
    # 7970
    # pants
    # 4176
    # jackets
    # 3936
    # bags
    # 3303
    # sweaters
    # 2389
    # swimwear
    # 1369
    # activewear
    # 1271
    # skirts
    # 1125

    # df_big = df_big[df_big["TYPES"] == "pendantnecklace"]
    # df_big = df_big[df_big["TYPES"] == "bracelet"]
    # df_big = df_big[df_big["TYPES"] == "diamondring"]  
     
    print("NUMBER OF LSBS:" + str(len(df_big)))

    df_big = df_big.reset_index(drop=True)

    cwd = os.getcwd()

    model_input_path = cwd + output_table_path
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_big, my_file, protocol=2)

    return model_input_path
