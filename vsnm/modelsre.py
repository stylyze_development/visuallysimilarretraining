import csv
import cv2
from datetime import datetime
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

# VGG 19 (the pre-trained model)
#
# Some possibile configurations:
#
# Image (224*224*3) --> relu5_4 (14*14*512)
# Image (224*224*3) --> pool5 (7*7*512)
# Image (224*224*3) --> relu7 (1*1*4096)
#
# Image (112*112*3) --> relu5_4 (7*7*512)

# we define our VGG19 network here
def vgg19_no_top(data_path, input_image):

    layers = (
        'conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',
        'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',
        'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3',
        'relu3_3', 'conv3_4', 'relu3_4', 'pool3',
        'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3',
        'relu4_3', 'conv4_4', 'relu4_4', 'pool4',
        'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3',
        'relu5_3', 'conv5_4', 'relu5_4', 'pool5'
    )
    data = scipy.io.loadmat(data_path)
    mean = data['normalization'][0][0][0]
    mean_pixel = np.mean(mean, axis=(0, 1))
    weights = data['layers'][0]
    net = {}
    current = input_image
    for i, name in enumerate(layers):
        kind = name[:4]
        if kind == 'conv':
            kernels, bias = weights[i][0][0][0][0]
            # matconvnet: weights are [width, height, in_channels, out_channels]
            # tensorflow: weights are [height, width, in_channels,
            # out_channels]
            kernels = np.transpose(kernels, (1, 0, 2, 3))
            bias = bias.reshape(-1)
            current = _conv_layer(current, kernels, bias)
        elif kind == 'relu':
            current = tf.nn.relu(current)
        elif kind == 'pool':
            current = _pool_layer(current)
        net[name] = current

    assert len(net) == len(layers)
    return net, mean_pixel

def vgg19_no_top_trainable(data_path, input_image):

    layers = (
        'conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',
        'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',
        'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3',
        'relu3_3', 'conv3_4', 'relu3_4', 'pool3',
        'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3',
        'relu4_3', 'conv4_4', 'relu4_4', 'pool4',
        'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3',
        'relu5_3', 'conv5_4', 'relu5_4', 'pool5'
    )
    data = scipy.io.loadmat(data_path)
    mean = data['normalization'][0][0][0]
    mean_pixel = np.mean(mean, axis=(0, 1))
    weights = data['layers'][0]
    net = {}
    current = input_image
    for i, name in enumerate(layers):
        kind = name[:5]
        if kind == 'conv1' or kind == 'conv2' or kind == 'conv3' or kind == 'conv4':
            print(name)
            print(kind)
            print(i)
            kernels, bias = weights[i][0][0][0][0]
            # matconvnet: weights are [width, height, in_channels, out_channels]
            # tensorflow: weights are [height, width, in_channels,
            # out_channels]
            kernels = np.transpose(kernels, (1, 0, 2, 3))
            bias = bias.reshape(-1)
            current = _conv_layer(current, kernels, bias)
        elif kind == 'conv5':
            print(name)
            print(kind)
            print(i)
            kernels, bias = weights[i][0][0][0][0]
            # matconvnet: weights are [width, height, in_channels, out_channels]
            # tensorflow: weights are [height, width, in_channels,
            # out_channels]
            kernels = np.transpose(kernels, (1, 0, 2, 3))
            bias = bias.reshape(-1)
            current = _conv_layer_trainable(current, kernels, bias, i)    

        kind = name[:4]    
        if kind == 'relu':
            current = tf.nn.relu(current)
        elif kind == 'pool':
            current = _pool_layer(current)
        net[name] = current

    assert len(net) == len(layers)
    return net, mean_pixel    


def vgg19(data_path, input_image):
    layers = (
        'conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',
        'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',
        'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3',
        'relu3_3', 'conv3_4', 'relu3_4', 'pool3',
        'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3',
        'relu4_3', 'conv4_4', 'relu4_4', 'pool4',
        'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3',
        'relu5_3', 'conv5_4', 'relu5_4', 'pool5',
        'fc6',     'relu6',
        'fc7',     'relu7'
    )
    data = scipy.io.loadmat(data_path)
    mean = data['normalization'][0][0][0]
    mean_pixel = np.mean(mean, axis=(0, 1))
    weights = data['layers'][0]
    net = {}
    current = input_image
    for i, name in enumerate(layers):
        kind = name[:4]
        if kind == 'conv':
            kernels, bias = weights[i][0][0][0][0]
            # matconvnet: weights are [width, height, in_channels, out_channels]
            # tensorflow: weights are [height, width, in_channels,
            # out_channels]
            kernels = np.transpose(kernels, (1, 0, 2, 3))
            # Convert bias.shape = (1,dim) to (dim,)
            bias = bias.reshape(-1)
            current = _conv_layer(current, kernels, bias)
        elif kind == 'relu':
            current = tf.nn.relu(current)
        elif kind == 'pool':
            current = _pool_layer(current)
        elif kind == 'drop':
            current = _drop_out_layer(current)
        elif kind == 'fc6' or 'fc7':
            kernels, bias = weights[i][0][0][0][0]
            # matconvnet: weights are [width, height, in_channels, out_channels]
            # tensorflow: weights are [height, width, in_channels,
            # out_channels]
            kernels = np.transpose(kernels, (1, 0, 2, 3))
            # Convert bias.shape = (1,dim) to (dim,)
            bias = bias.reshape(bias.size)
            current = _fc_layer(current, kernels, bias)
        net[name] = current

    assert len(net) == len(layers)
    return net, mean_pixel


def _conv_layer(input, weights, bias):
    # CONV2D: stride of 1, padding 'SAME'
    conv = tf.nn.conv2d(input, tf.constant(weights), strides=(1, 1, 1, 1),
                        padding='SAME')
    return tf.nn.bias_add(conv, bias)

def _conv_layer_trainable(input, weights, bias, i):
    # CONV2D: stride of 1, padding 'SAME'
    
    weignts_ini = tf.get_variable("w" + str(i), initializer = tf.constant(weights))
    conv = tf.nn.conv2d(input, weignts_ini, strides=(1, 1, 1, 1),
                        padding='SAME')
    bias_ini = tf.get_variable("b" + str(i), initializer = bias)
    return tf.nn.bias_add(conv, bias_ini)

def _pool_layer(input):
    # MAXPOOL: window 2x2, sride 2, padding 'SAME'
    return tf.nn.max_pool(input, ksize=(1, 2, 2, 1), strides=(1, 2, 2, 1),
                          padding='SAME')

def _drop_out_layer(input):
    # DROP-OUT
    return tf.nn.dropout(input, 0.5)


def _fc_layer(input, weights, bias):
    # CONV2D: stride of 1, padding 'VALID'
    # Note that we use conv2d to implement the fully-connected layer
    W = tf.constant(weights)
    b = tf.constant(bias)
    cnn = tf.nn.conv2d(input, filter=W, strides=[
                       1, 1, 1, 1], padding='VALID')
    output = cnn + b
    return output
    # dim_w = weights.get_shape().as_list()[0]
    # dim_h = weights.get_shape().as_list()[1]
    # dim_in = weights.get_shape().as_list()[2]
    # dim_out = weights.get_shape().as_list()[3]
    # # Input
    # input_r = input
    # # Vectorize
    # dense1 = tf.reshape(input_r, [-1, dim_w*dim_h*dim_in])
    # # Fc1
    # fc1 = tf.add(tf.matmul(dense1, weights), bias)


def preprocess(image, mean_pixel):
    return image - mean_pixel


def unprocess(image, mean_pixel):
    return image + mean_pixel

# weights  = {
#     'wd1': tf.Variable(tf.random_normal([7*7*512, net_3_dim], stddev=0.1), name = "wd1"),
#     # 'wd2': tf.Variable(tf.random_normal([1024, n_output], stddev=0.1))
# }
# biases   = {
#     'bd1': tf.Variable(tf.random_normal([net_3_dim], stddev=0.1), name = "bd1"),
#     # 'bd2': tf.Variable(tf.random_normal([n_output], stddev=0.1))
# }
# def conv_basic(_input, _w, _b, _keepratio):
#     # Input
#     _input_r = _input
#     # Vectorize
#     _dense1 = tf.reshape(_input_r, [-1, _w['wd1'].get_shape().as_list()[0]])
#     # Fc1
#     _fc1 = tf.nn.relu(tf.add(tf.matmul(_dense1, _w['wd1']), _b['bd1']))
#     # _fc_dr1 = tf.nn.dropout(_fc1, _keepratio)
#     _out = tf.nn.dropout(_fc1, _keepratio)
#     # Fc2
#     # _out = tf.add(tf.matmul(_fc_dr1, _w['wd2']), _b['bd2'])
#     # Return everything
#     out = {'input_r': _input_r, 'dense1': _dense1,
#         'fc1': _fc1, 'out': _out }
#     # out = {'input_r': _input_r, 'dense1': _dense1,
#     #     'fc1': _fc1, 'fc_dr1': _fc_dr1, 'out': _out }
#     return out


def initialize_parameters():
    # tf.set_random_seed(1)
    W1 = tf.get_variable(
        "W1", [4, 4, 3, 8], initializer=tf.contrib.layers.xavier_initializer(seed=0))
    W2 = tf.get_variable(
        "W2", [2, 2, 8, 16], initializer=tf.contrib.layers.xavier_initializer(seed=0))
    parameters = {"W1": W1, "W2": W2}
    return parameters


def forward_propagation(X, parameters, keepratio_tf, net_3_dim):

    W1 = parameters['W1']
    W2 = parameters['W2']

    # CONV2D: stride of 1, padding 'SAME'
    Z1 = tf.nn.conv2d(X, W1, strides=[1, 1, 1, 1], padding='SAME')
    # RELU
    A1 = tf.nn.relu(Z1)
    # MAXPOOL: window 8x8, sride 8, padding 'SAME'
    P1_a = tf.nn.max_pool(A1, ksize=[1, 8, 8, 1], strides=[
                          1, 8, 8, 1], padding='SAME')
    P1 = tf.nn.dropout(P1_a, keepratio_tf)  # DROP-OUT here
    # CONV2D: filters W2, stride 1, padding 'SAME'
    Z2 = tf.nn.conv2d(P1, W2, strides=[1, 1, 1, 1], padding='SAME')
    # RELU
    A2 = tf.nn.relu(Z2)
    # MAXPOOL: window 4x4, stride 4, padding 'SAME'
    P2_a = tf.nn.max_pool(A2, ksize=[1, 4, 4, 1], strides=[
                          1, 4, 4, 1], padding='SAME')
    P2 = tf.nn.dropout(P2_a, keepratio_tf)  # DROP-OUT here
    # FLATTEN
    PP2 = tf.contrib.layers.flatten(P2)
    # FULLY-CONNECTED without non-linear activation function (not call
    # softmax).
    Z3 = tf.contrib.layers.fully_connected(
        PP2, net_3_dim, activation_fn=None, scope="net1", reuse=None)

    return Z3


def forward_propagation_2(X, parameters, keepratio_tf, net_3_dim):

    W1 = parameters['W1']
    W2 = parameters['W2']

    # CONV2D: stride of 1, padding 'SAME'
    Z1 = tf.nn.conv2d(X, W1, strides=[1, 1, 1, 1], padding='SAME')
    # RELU
    A1 = tf.nn.relu(Z1)
    # MAXPOOL: window 8x8, sride 8, padding 'SAME'
    P1_a = tf.nn.max_pool(A1, ksize=[1, 8, 8, 1], strides=[
                          1, 8, 8, 1], padding='SAME')
    P1 = tf.nn.dropout(P1_a, keepratio_tf)  # DROP-OUT here
    # CONV2D: filters W2, stride 1, padding 'SAME'
    Z2 = tf.nn.conv2d(P1, W2, strides=[1, 1, 1, 1], padding='SAME')
    # RELU
    A2 = tf.nn.relu(Z2)
    # MAXPOOL: window 4x4, stride 4, padding 'SAME'
    P2_a = tf.nn.max_pool(A2, ksize=[1, 4, 4, 1], strides=[
                          1, 4, 4, 1], padding='SAME')
    P2 = tf.nn.dropout(P2_a, keepratio_tf)  # DROP-OUT here
    # FLATTEN
    PP2 = tf.contrib.layers.flatten(P2)
    # FULLY-CONNECTED without non-linear activation function (not call
    # softmax).
    Z3 = tf.contrib.layers.fully_connected(
        PP2, net_3_dim, activation_fn=None, scope="net1", reuse=True)

    return Z3


def linear_encoding(X, net_3_dim):
    embedding = tf.contrib.layers.fully_connected(
        X, net_3_dim, activation_fn=None, scope="net2", reuse=None)
    return embedding


def linear_encoding_2(X, net_3_dim):
    embedding = tf.contrib.layers.fully_connected(
        X, net_3_dim, activation_fn=None, scope="net2", reuse=True)
    return embedding
