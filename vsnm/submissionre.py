import numpy as np
import os
import pandas as pd
import pickle
import sys


def output_lsbs(model_input_path, model_output_path, model_output_path_2, output_table_path_3):

    # Load the visually similar search result
    with open(model_output_path, "rb") as my_file:
        deep_learning_dic = pickle.load(my_file)

    # Load the visually similar search score result
    with open(model_output_path_2, "rb") as my_file2:
        deep_learning_score_dic = pickle.load(my_file2)

    # Load the big table as a reference to get the type, style, url, etc.
    with open(model_input_path, "rb") as my_file3:
        deep_learning_df = pickle.load(my_file3)

    # check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "prod201430045-RED"]
    # print(check_id["CURATOR_IMAGE_PATH"])
    # print(check_id["TYPES"])
    # check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "prod202590040-GOLDSILVER"]
    # print(check_id["CURATOR_IMAGE_PATH"])
    # print(check_id["TYPES"])
    # check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "prod207530043-BLACK"]
    # print(check_id["CURATOR_IMAGE_PATH"])
    # print(check_id["TYPES"])
     

    output = {}
    output_score = {}

    # List of items contains the indexes for the seed items of interest for
    # debugging purpose
    # list_of_items = range(0, 2000)

    # list_of_items_train = ["29282", "47165"] 
    # list_of_items_test = ['38308', '43717', '38308', '43717', '38308', '14866', '14866', '2563', '2563', '57122', '23616', '38308', '43717', '38308', '14866', '23616', '23616', '23616', '14866', '12903', '38308', '23616', '57122', '38308', '14375', '57122', '23616', '43717', '38308', '57122', '57122', '57122', '12903', '57122', '12903', '2563', '14866', '43717', '14866', '14866', '38308', '23616', '46035', '23616', '23616', '23616', '2563', '38308', '38308', '57122', '2563', '14866', '14866', '38308', '38308', '14866', '57122', '23616', '14866', '23616', '23616', '23616', '14866', '14866', '23616', '7928', '23616', '12903', '2563', '2563', '57122', '43717', '43717', '2563', '2563', '23616', '43717', '2563', '2563', '7928', '14866', '2563', '57122', '14866', '38308', '23616', '57122', '2563', '46035', '43717', '23616', '38308', '38308', '46035', '23616', '14866', '23616', '14866', '43717', '14866'] 
    # list_of_items_test = list(set(list_of_items_test) - set(list_of_items_train))
    # list_of_items_2 = list_of_items_test
     
    # list_of_items = []
    # for i in list_of_items_2:
    #     item = int(i)
    #     list_of_items.append(item)

    for key in deep_learning_dic.keys():
        for item_i in range(len(deep_learning_dic[key])):
            index = deep_learning_dic[key][str(item_i)][0]
            output[str(index)] = deep_learning_dic[key][str(item_i)]
            output_score[str(index)] = deep_learning_score_dic[
                key][str(item_i)]
            # if index in list_of_items:
            #     output[str(index)] = deep_learning_dic[key][str(item_i)]
            #     output_score[str(index)] = deep_learning_score_dic[key][str(item_i)]

    # Output url, index, variant id, type, style for the list of similar items
    output_2 = {}
    output_3 = []
    output_4 = []
    output_5 = {}
    output_6 = []
    output_7 = []
    output_8 = []
    output_9 = []
    output_10 = []
    for key in output.keys():
        output_2[key] = deep_learning_df["IMAGE_URL"][output[key]].tolist()
        output_3.append(output_2[key])
        output_4.append(output[key])
        output_5[key] = deep_learning_df["VARIANT_ID"][output[key]].tolist()
        output_6.append(output_5[key])
        output_7.append(deep_learning_df["TYPES"][output[key]].tolist()[0])
        output_8.append(deep_learning_df["STYLES"][output[key]].tolist()[0])
        output_9.append(output_score[key])
        output_10.append(deep_learning_df["CATEGORIES"][output[key]].tolist()[0])

    # print(output_6)    

    # We can upload the csv files to google spreadsheet to visualize the result
    cwd = os.getcwd()
    table = pd.DataFrame(output_3, columns=[
                         '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                         '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table.to_csv(cwd + output_table_path_3 + "url.csv")

    table_index = pd.DataFrame(
        output_4, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_index.to_csv(cwd + output_table_path_3 + "index.csv")

    table_id = pd.DataFrame(
        output_6, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_id.to_csv(cwd + output_table_path_3 + "id.csv")

    table_type = pd.DataFrame(output_7, columns=['TYPE'])
    table_type.to_csv(cwd + output_table_path_3 + "type.csv")

    table_style = pd.DataFrame(output_8, columns=['STYLE'])
    table_style.to_csv(cwd + output_table_path_3 + "style.csv")

    table_score = pd.DataFrame(
        output_9, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])

    table_score.to_csv(cwd + output_table_path_3 + "score.csv")

    table_categories = pd.DataFrame(output_10, columns=['CATEGORY'])
    table_categories.to_csv(cwd + output_table_path_3 + "category.csv")

    # Output file path of id.csv
    # id.csv is the output file to the backend team
    # output_csv_path = `python evaluation.py input_csv_path`
    # sys.stdout.write(cwd + output_table_path_3 + "id.csv")
    # sys.exit(0)
