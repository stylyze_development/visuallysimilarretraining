import csv
import numpy as np
import pandas as pd
import pickle
import re
import os

import vsnm.traincsndata as model
# import vsnm.trainfine as model
import vsnm.configurationre as configuration

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():

    cwd = os.getcwd()
    args = configuration.parse_arguments()

    model.embedding(args["output_table_path_4"],
                    args["n_H0"],
                    args["n_W0"],
                    args["n_C0"],
                    args["batch_size"],
                    args["training_ratio"],
                    args["embedding_size"],
                    args["image_normalization"],
                    args["same_image_epsilon"],
                    args["model_weight_path"],
                    args["model_weight_path_2"],
                    args["model_name"],
                    args["output_layer"],
                    args["num_epoch"],
                    args["MARGIN"],
                    args["net_3_dim"],
                    args["learning_rate"],
                    args["keepratio"],
                    14400,
                    320,
                    args["output_table_path"])


if __name__ == '__main__':
    main()
