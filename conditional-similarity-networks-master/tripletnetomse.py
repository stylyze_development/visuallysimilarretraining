import torch
import torch.nn as nn
import torch.nn.functional as F

class CS_Tripletnet(nn.Module):
    def __init__(self, embeddingnet_1, embeddingnet_2):
        super(CS_Tripletnet, self).__init__()
        self.embeddingnet_1 = embeddingnet_1
        self.embeddingnet_2 = embeddingnet_2

    def forward(self, x, y, z):
        """ x: Anchor image,
            y: Distant (negative) image,
            z: Close (positive) image,
            c: Integer indicating according to which notion of similarity images are compared"""
        embedded_x_1 = self.embeddingnet_1(x)
        embedded_y_1 = self.embeddingnet_1(y)
        embedded_z_1 = self.embeddingnet_1(z)

        embedded_x_2 = self.embeddingnet_2(x)
        embedded_y_2 = self.embeddingnet_2(y)
        embedded_z_2 = self.embeddingnet_2(z)

        embedded_x = torch.cat((embedded_x_1, embedded_x_2), 0)
        embedded_y = torch.cat((embedded_y_1, embedded_y_2), 0)
        embedded_z = torch.cat((embedded_z_1, embedded_z_2), 0)

        dist_a = F.pairwise_distance(embedded_x, embedded_y, 2)  
        dist_b = F.pairwise_distance(embedded_x, embedded_z, 2)  

        dist_a_11 = F.pairwise_distance(embedded_x_1, embedded_y_1, 2) # same attention, different class
        dist_b_11 = F.pairwise_distance(embedded_x_1, embedded_z_1, 2) # same attention, same class

        dist_a_12 = F.pairwise_distance(embedded_x_1, embedded_y_2, 2) # different attention, different class
        dist_b_12 = F.pairwise_distance(embedded_x_1, embedded_z_2, 2) # differenct attention, same class

        dist_a_21 = F.pairwise_distance(embedded_x_2, embedded_y_1, 2) # different attention, different class
        dist_b_21 = F.pairwise_distance(embedded_x_2, embedded_z_1, 2) # differenct attention, same class

        dist_a_22 = F.pairwise_distance(embedded_x_2, embedded_y_2, 2) # same attention, different class
        dist_b_22 = F.pairwise_distance(embedded_x_2, embedded_z_2, 2) # same attention, same class

        return dist_a_11, dist_b_11, dist_a_12, dist_b_12, dist_a_21, dist_b_21, dist_a_22, dist_b_22, dist_a, dist_b