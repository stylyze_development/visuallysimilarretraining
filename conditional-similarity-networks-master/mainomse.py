from __future__ import print_function
import argparse
import os
import sys
import shutil
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import transforms
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
from triplet_image_loader import TripletImageLoader
# from tripletnet import CS_Tripletnet
from tripletnetomse import CS_Tripletnet
from visdom import Visdom
import numpy as np
# import Resnet_18
import Resnet_x_omse
from omse import OMSE_1, OMSE_2
# from csn import ConditionalSimNet

# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--epochs', type=int, default=60, metavar='N',
                    help='number of epochs to train (default: 200)')
parser.add_argument('--start_epoch', type=int, default=1, metavar='N',
                    help='number of start epoch (default: 1)')
parser.add_argument('--lr', type=float, default=5e-5, metavar='LR',
                    help='learning rate (default: 5e-5)')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--log-interval', type=int, default=20, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--margin', type=float, default=1.0, metavar='M',
                    help='margin for triplet loss (default: 0.2)')
parser.add_argument('--resume', default='', type=str,
                    help='path to latest checkpoint (default: none)')
parser.add_argument('--name', default='Conditional_Similarity_Network', type=str,
                    help='name of experiment')
parser.add_argument('--embed_loss', type=float, default=5e-3, metavar='M',
                    help='parameter for loss for embedding norm')
parser.add_argument('--mask_loss', type=float, default=5e-4, metavar='M',
                    help='parameter for loss for mask norm')
parser.add_argument('--num_traintriplets', type=int, default=50000, metavar='N',
                    help='how many unique training triplets (default: 100000)')
parser.add_argument('--dim_embed', type=int, default=256, metavar='N',
                    help='how many dimensions in embedding (default: 64)')
parser.add_argument('--test', dest='test', action='store_true',
                    help='To only run inference on test set')
parser.add_argument('--learned', dest='learned', action='store_true',
                    help='To learn masks from random initialization')
parser.add_argument('--prein', dest='prein', action='store_true',
                    help='To initialize masks to be disjoint')
parser.add_argument('--visdom', dest='visdom', action='store_true',
                    help='Use visdom to track and plot')
parser.add_argument('--conditions', nargs='*', type=int,
                    help='Set of similarity notions')
parser.set_defaults(test=False)
parser.set_defaults(learned=False)
parser.set_defaults(prein=False)
parser.set_defaults(visdom=False)

best_loss = 30


def main():
    global args, best_loss
    args = parser.parse_args()
    args.cuda = not args.no_cuda and torch.cuda.is_available()
    torch.manual_seed(args.seed)
    if args.cuda:
        torch.cuda.manual_seed(args.seed)
    if args.visdom:
        global plotter
        plotter = VisdomLinePlotter(env_name=args.name)

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    global conditions
    if args.conditions is not None:
        conditions = args.conditions
    else:
        # conditions = [0,1,2,3]
        conditions = [0]

    kwargs = {'num_workers': 4, 'pin_memory': True} if args.cuda else {}
    train_loader = torch.utils.data.DataLoader(
        TripletImageLoader('data', 'ut-zap50k-images', 'filenames.json',
                           conditions, 'train', n_triplets=100000,
                           transform=transforms.Compose([
                               transforms.Resize(224),
                               transforms.CenterCrop(224),
                               transforms.RandomHorizontalFlip(),
                               transforms.ToTensor(),
                               normalize,
                           ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        TripletImageLoader('data', 'ut-zap50k-images', 'filenames.json',
                           conditions, 'test', n_triplets=80000,
                           transform=transforms.Compose([
                               transforms.Resize(224),
                               transforms.CenterCrop(224),
                               transforms.ToTensor(),
                               normalize,
                           ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    val_loader = torch.utils.data.DataLoader(
        TripletImageLoader('data', 'ut-zap50k-images', 'filenames.json',
                           conditions, 'val', n_triplets=80000,
                           transform=transforms.Compose([
                               transforms.Resize(224),
                               transforms.CenterCrop(224),
                               transforms.ToTensor(),
                               normalize,
                           ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)

    # model = Resnet_18.resnet18(pretrained=True, embedding_size=args.dim_embed)
    model = Resnet_x_omse.resnet18(pretrained=True)
    omse_model_1 = OMSE_1(model)
    omse_model_2 = OMSE_2(model)

    # csn_model_1 = ConditionalSimNet(omse_model_1, n_conditions=len(conditions),
    #     embedding_size=args.dim_embed, learnedmask=args.learned, prein=args.prein)
    # global mask_var_1
    # mask_var_1 = csn_model_1.masks.weight

    # csn_model_2 = ConditionalSimNet(omse_model_2, n_conditions=len(conditions),
    #     embedding_size=args.dim_embed, learnedmask=args.learned, prein=args.prein)
    # global mask_var_2
    # mask_var_2 = csn_model_2.masks.weight

    tnet = CS_Tripletnet(omse_model_1, omse_model_2)
    if args.cuda:
        tnet.cuda()

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            tnet.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True

    criterion = torch.nn.MarginRankingLoss(margin=args.margin)
    parameters = filter(lambda p: p.requires_grad, tnet.parameters())
    optimizer = optim.Adam(parameters, lr=args.lr)

    n_parameters = sum([p.data.nelement() for p in tnet.parameters()])
    print('  + Number of params: {}'.format(n_parameters))

    if args.test:
        test_loss = test(test_loader, tnet, criterion, 1)
        sys.exit()

    for epoch in range(args.start_epoch, args.epochs + 1):
        # update learning rate
        adjust_learning_rate(optimizer, epoch)
        # train for one epoch
        loss_train = train(train_loader, tnet, criterion, optimizer, epoch)
        # evaluate on validation set
        loss = test(test_loader, tnet, criterion, epoch)

        # if epoch == args.epochs:
        # is_best = True
        # remember best acc and save checkpoint
        is_best = loss < best_loss
        best_loss = min(loss, best_loss)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': tnet.state_dict(),
            'best_prec1': best_loss,
        }, is_best)


def train(train_loader, tnet, criterion, optimizer, epoch):
    losses = AverageMeter()
    accs = AverageMeter()
    accs_11 = AverageMeter()
    accs_22 = AverageMeter()
    # emb_norms = AverageMeter()
    # mask_norms = AverageMeter()

    # switch to train mode
    tnet.train()
    for batch_idx, (data1, data2, data3, c) in enumerate(train_loader):
        if args.cuda:
            data1, data2, data3 = data1.cuda(), data2.cuda(), data3.cuda()
        data1, data2, data3 = Variable(data1), Variable(data2), Variable(data3)

        # dist_a_11  # same attention, different class
        # dist_b_11  # same attention, same class

        # dist_a_12  # different attention, different class
        # dist_b_12  # differenct attention, same class

        # dist_a_21  # different attention, different class
        # dist_b_21  # differenct attention, same class

        # dist_a_22  # same attention, different class
        # dist_b_22  # same attention, same class

        # compute output
        dist_a_11, dist_b_11, dist_a_12, dist_b_12, dist_a_21, dist_b_21, dist_a_22, dist_b_22, dist_a, dist_b = tnet(
            data1, data2, data3)
        # 1 means, dista should be larger than distb
        target = torch.FloatTensor(dist_a_11.size()).fill_(1)
        if args.cuda:
            target = target.cuda()
        target = Variable(target)

        loss_triplet_1 = criterion(dist_a_11, dist_b_11, target)
        loss_triplet_2 = criterion(dist_a_12, dist_b_11, target)
        loss_triplet_3 = criterion(dist_b_12, dist_b_11, target)
        loss_triplet_4 = criterion(dist_a_21, dist_b_11, target)
        loss_triplet_5 = criterion(dist_b_21, dist_b_11, target)
        loss_triplet_6 = criterion(dist_a_22, dist_b_11, target)

        loss_triplet_7 = criterion(dist_a_11, dist_b_22, target)
        loss_triplet_8 = criterion(dist_a_12, dist_b_22, target)
        loss_triplet_9 = criterion(dist_b_12, dist_b_22, target)
        loss_triplet_10 = criterion(dist_a_21, dist_b_22, target)
        loss_triplet_11 = criterion(dist_b_21, dist_b_22, target)
        loss_triplet_12 = criterion(dist_a_22, dist_b_22, target)

        loss_triplet_13 = criterion(dist_a_12, dist_a_11, target)
        loss_triplet_14 = criterion(dist_a_21, dist_a_11, target)

        loss_triplet_15 = criterion(dist_a_12, dist_a_22, target)
        loss_triplet_16 = criterion(dist_a_21, dist_a_22, target)

        loss_triplet_17 = criterion(dist_a_12, dist_b_12, target)
        loss_triplet_18 = criterion(dist_a_21, dist_b_12, target)

        loss_triplet_19 = criterion(dist_a_12, dist_b_21, target)
        loss_triplet_20 = criterion(dist_a_21, dist_b_21, target)

        loss = loss_triplet_1 + loss_triplet_2 + loss_triplet_3 + loss_triplet_4 + loss_triplet_5 + \
            loss_triplet_6 + loss_triplet_7 + loss_triplet_8 + \
            loss_triplet_9 + loss_triplet_10 + \
            loss_triplet_11 + loss_triplet_12 + loss_triplet_13 + loss_triplet_14 + loss_triplet_15 + \
            loss_triplet_16 + loss_triplet_17 + loss_triplet_18 + \
            loss_triplet_19 + loss_triplet_20

        # measure accuracy and record loss
        acc = accuracy(dist_a, dist_b)
        acc_11 = accuracy(dist_a_11, dist_b_11)
        acc_22 = accuracy(dist_a_22, dist_b_22)
        losses.update(loss, data1.size(0))
        # losses.update(loss_triplet.data[0], data1.size(0))
        accs.update(acc, data1.size(0))
        accs_11.update(acc_11, data1.size(0))
        accs_22.update(acc_22, data1.size(0))
        # emb_norms.update(loss_embedd)
        # mask_norms.update(loss_mask)
        # emb_norms.update(loss_embedd.data[0])
        # mask_norms.update(loss_mask.data[0])

        # compute gradient and do optimizer step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{}]\t'
                  'Loss: {:.4f} ({:.4f}) \t'
                  'Acc : {:.2f}% ({:.2f}%) \t'
                  'Acc Part 1: {:.2f}% ({:.2f}%) \t'
                  'Acc Part 2: {:.2f}% ({:.2f}%) \t'.format(
                      epoch, batch_idx * len(data1), len(train_loader.dataset),
                      losses.val, losses.avg,
                  100. * accs.val, 100. * accs.avg,    
                  100. * accs_11.val, 100. * accs_11.avg,
                  100. * accs_22.val, 100. * accs_22.avg))

    # # log avg values to visdom
    # if args.visdom:
    #     plotter.plot('acc', 'train', epoch, accs.avg)
    #     plotter.plot('loss', 'train', epoch, losses.avg)
    #     plotter.plot('emb_norms', 'train', epoch, emb_norms.avg)
    #     plotter.plot('mask_norms', 'train', epoch, mask_norms.avg)
    #     if epoch % 10 == 0:
    #         plotter.plot_mask(torch.nn.functional.relu(
    #             mask_var).data.cpu().numpy().T, epoch)

    return losses.avg


def test(test_loader, tnet, criterion, epoch):
    losses = AverageMeter()
    accs = AverageMeter()
    accs_11 = AverageMeter()
    accs_22 = AverageMeter()
    # accs_cs = {}
    # for condition in conditions:
    #     accs_cs[condition] = AverageMeter()

    # switch to evaluation mode
    with torch.no_grad():
        tnet.eval()
        for batch_idx, (data1, data2, data3, c) in enumerate(test_loader):
            if args.cuda:
                data1, data2, data3 = data1.cuda(), data2.cuda(), data3.cuda()
            data1, data2, data3 = Variable(data1), Variable(
                data2), Variable(data3)
            # c_test = c

            # compute output
            dist_a_11, dist_b_11, dist_a_12, dist_b_12, dist_a_21, dist_b_21, dist_a_22, dist_b_22, dist_a, dist_b = tnet(
                data1, data2, data3)
            target = torch.FloatTensor(dist_a_11.size()).fill_(1)
            if args.cuda:
                target = target.cuda()
            target = Variable(target)
            # test_loss = criterion(dista, distb, target)
            # test_loss =  criterion(dista, distb, target).data[0]
            loss_triplet_1 = criterion(dist_a_11, dist_b_11, target)
            loss_triplet_2 = criterion(dist_a_12, dist_b_11, target)
            loss_triplet_3 = criterion(dist_b_12, dist_b_11, target)
            loss_triplet_4 = criterion(dist_a_21, dist_b_11, target)
            loss_triplet_5 = criterion(dist_b_21, dist_b_11, target)
            loss_triplet_6 = criterion(dist_a_22, dist_b_11, target)

            loss_triplet_7 = criterion(dist_a_11, dist_b_22, target)
            loss_triplet_8 = criterion(dist_a_12, dist_b_22, target)
            loss_triplet_9 = criterion(dist_b_12, dist_b_22, target)
            loss_triplet_10 = criterion(dist_a_21, dist_b_22, target)
            loss_triplet_11 = criterion(dist_b_21, dist_b_22, target)
            loss_triplet_12 = criterion(dist_a_22, dist_b_22, target)

            loss_triplet_13 = criterion(dist_a_12, dist_a_11, target)
            loss_triplet_14 = criterion(dist_a_21, dist_a_11, target)

            loss_triplet_15 = criterion(dist_a_12, dist_a_22, target)
            loss_triplet_16 = criterion(dist_a_21, dist_a_22, target)

            loss_triplet_17 = criterion(dist_a_12, dist_b_12, target)
            loss_triplet_18 = criterion(dist_a_21, dist_b_12, target)

            loss_triplet_19 = criterion(dist_a_12, dist_b_21, target)
            loss_triplet_20 = criterion(dist_a_21, dist_b_21, target)

            test_loss = loss_triplet_1 + loss_triplet_2 + loss_triplet_3 + loss_triplet_4 + loss_triplet_5 + \
                loss_triplet_6 + loss_triplet_7 + loss_triplet_8 + \
                loss_triplet_9 + loss_triplet_10 + \
                loss_triplet_11 + loss_triplet_12 + loss_triplet_13 + loss_triplet_14 + loss_triplet_15 + \
                loss_triplet_16 + loss_triplet_17 + loss_triplet_18 + \
                loss_triplet_19 + loss_triplet_20

            acc = accuracy(dist_a, dist_b)
            acc_11 = accuracy(dist_a_11, dist_b_11)
            acc_22 = accuracy(dist_a_22, dist_b_22)
             
            accs.update(acc, data1.size(0))
            accs_11.update(acc_11, data1.size(0))
            accs_22.update(acc_22, data1.size(0))    

            # measure accuracy and record loss
            # acc = accuracy(dista, distb)
            # accs.update(acc, data1.size(0))
            # for condition in conditions:
            #     accs_cs[condition].update(accuracy_id(
            #         dista, distb, c_test, condition), data1.size(0))
            losses.update(test_loss, data1.size(0))

    print('\nTest set Loss: {:.4f} ({:.4f}) \t'
          'Acc: {:.2f}% ({:.2f}%) \t'
          'Acc Part 1: {:.2f}% ({:.2f}%) \t'
          'Acc Part 2: {:.2f}% ({:.2f}%) \t'.format(losses.val, losses.avg,
           100. * accs.val, 100. * accs.avg,
           100. * accs_11.val, 100. * accs_11.avg,
           100. * accs_22.val, 100. * accs_22.avg)) 
     
    # if args.visdom:
    #     for condition in conditions:
    #         plotter.plot('accs', 'acc_{}'.format(condition),
    #                      epoch, accs_cs[condition].avg)
    #     plotter.plot(args.name, args.name, epoch, accs.avg, env='overview')
    #     plotter.plot('acc', 'test', epoch, accs.avg)
    #     plotter.plot('loss', 'test', epoch, losses.avg)
    return losses.avg


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    """Saves checkpoint to disk"""
    directory = "runs/%s/" % (args.name)
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = directory + filename
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'runs/%s/' %
                        (args.name) + 'model_best.pth.tar')


class VisdomLinePlotter(object):
    """Plots to Visdom"""

    def __init__(self, env_name='main'):
        self.viz = Visdom()
        self.env = env_name
        self.plots = {}

    def plot(self, var_name, split_name, x, y, env=None):
        if env is not None:
            print_env = env
        else:
            print_env = self.env
        if var_name not in self.plots:
            self.plots[var_name] = self.viz.line(X=np.array([x, x]), Y=np.array([y, y]), env=print_env, opts=dict(
                legend=[split_name],
                title=var_name,
                xlabel='Epochs',
                ylabel=var_name
            ))
        else:
            self.viz.updateTrace(X=np.array([x]), Y=np.array(
                [y]), env=print_env, win=self.plots[var_name], name=split_name)

    def plot_mask(self, masks, epoch):
        self.viz.bar(
            X=masks,
            env=self.env,
            opts=dict(
                stacked=True,
                title=epoch,
            )
        )


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * ((1 - 0.015) ** epoch)
    if args.visdom:
        plotter.plot('lr', 'learning rate', epoch, lr)
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(dista, distb):
    margin = 0
    pred = (dista - distb - margin).cpu().data
    return (pred > 0.0).sum().float() * (1.0 / dista.size()[0])
    # return (pred > 0).sum()*1.0/dista.size()[0]


def accuracy_id(dista, distb, c, c_id):
    margin = 0
    pred = (dista - distb - margin).cpu().data
    return ((pred > 0) * (c.cpu().data == c_id)).sum() * 1.0 / (c.cpu().data == c_id).sum()

if __name__ == '__main__':
    main()
