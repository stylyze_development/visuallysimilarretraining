import torch.nn as nn
import torch.nn.functional as F


class OMSE_1(nn.Module):

    def __init__(self, embeddingnet):
        super(OMSE_1, self).__init__()
        self.embeddingnet = embeddingnet
        self.pool = nn.AvgPool2d(kernel_size = 7)
        self.fc1 = nn.Linear(512, 32)
        self.fc2 = nn.Linear(32, 512)
        self.fc3 = nn.Linear(512 * 7 * 7, 256)

    def forward(self, x):
        x_embd = self.embeddingnet(x)
        x_1 = self.pool(x_embd)
        x_2 = x_1.view(-1, 512 * 1 * 1)
        x_3 = F.relu(self.fc1(x_2))
        x_4 = F.sigmoid(self.fc2(x_3))
        x_4 = x_4.view(-1, 512, 1, 1)
        x_5 = x_embd * x_4
        # x_5 = x_embd
        x_6 = x_5.view(-1, 512 * 7 * 7)
        # x_7 = x_6
        x_7 = self.fc3(x_6)
        return x_7

class OMSE_2(nn.Module):

    def __init__(self, embeddingnet):
        super(OMSE_2, self).__init__()
        self.embeddingnet = embeddingnet
        self.pool = nn.AvgPool2d(kernel_size = 7)
        self.fc1 = nn.Linear(512, 32)
        self.fc2 = nn.Linear(32, 512)
        self.fc3 = nn.Linear(512 * 7 * 7, 256)

    def forward(self, x):
        x_embd = self.embeddingnet(x)
        x_1 = self.pool(x_embd)
        x_2 = x_1.view(-1, 512 * 1 * 1)
        x_3 = F.relu(self.fc1(x_2))
        x_4 = F.sigmoid(self.fc2(x_3))
        x_4 = x_4.view(-1, 512, 1, 1)
        x_5 = x_embd * x_4
        # x_5 = x_embd
        x_6 = x_5.view(-1, 512 * 7 * 7)
        # x_7 = x_6
        x_7 = self.fc3(x_6)
        return x_7        

    

 