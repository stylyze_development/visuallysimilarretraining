import csv
import numpy as np
import pandas as pd
import pickle
import re
import os

import vsnm.traindatacate2 as data
import vsnm.train as model
# import vsnm.trainfine as model
import vsnm.configurationre as configuration
import vsnm.checkbox as checkbox
import vsnm.precheckbox as precheckbox
import vsnm.traindataaug as augmentation
import vsnm.traintestre as dataset
# import vsnm.evaldatare as datatable
import vsnm.evaldataretarget as datatable

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():

    cwd = os.getcwd()
    args = configuration.parse_arguments()

    path_train, path_test = dataset.load_data(args["database_path"])

    model_input_path = datatable.load_data(path_train,
                                           args["output_table_path"],
                                           args["column_names"])

    # # augmentation.data_aug(args["output_table_path"])

    num_items = precheckbox.parse_data(args["output_table_path"],
                                       args["output_table_path_7"],
                                       args["output_table_path_8"],
                                       args["output_table_path_13"],
                                       args["search_size_2"])

    # num_items = 41

    checkbox.parse_data(args["output_table_path_5"],
                        args["output_table_path_6"],
                        args["output_table_path_7"],
                        args["output_table_path_8"],
                        args["output_table_path_10"],
                        args["output_table_path_11"],
                        num_items)

    data.load_data(args["output_table_path"],
                   args["output_table_path_4"],
                   args["output_table_path_5"],
                   args["output_table_path_6"],
                   args["column_names"],
                   num_items,
                   train_or_test = "train")

    with open(cwd + args["output_table_path_4"], "rb") as my_file:
        df_triplet_final_train = pickle.load(my_file)

    data.load_data(args["output_table_path"],
                   args["output_table_path_4"],
                   args["output_table_path_10"],
                   args["output_table_path_11"],
                   args["column_names"],
                   num_items,
                   train_or_test = "test")

    with open(cwd + args["output_table_path_4"], "rb") as my_file:
        df_triplet_final_test = pickle.load(my_file)    

    print("len(df_triplet_final_train) = ", len(df_triplet_final_train))
    print("len(df_triplet_final_test) = ", len(df_triplet_final_test))

    train_size = int(len(df_triplet_final_train)/32)*32
    test_size = int(len(df_triplet_final_test)/32)*32

    print("train_size = ", train_size)
    print("test_size = ", test_size)

    df_triplet_final = df_triplet_final_train.append(df_triplet_final_test, ignore_index=True)
     
    # print(df_triplet_final)
    print("len(df_triplet_final) = ", len(df_triplet_final)) 

    with open(cwd + args["output_table_path_4"], "wb") as my_file3:
        pickle.dump(df_triplet_final, my_file3, protocol=2)
          
    print("******************************************************************")   

    model.embedding(args["output_table_path_4"],
                    args["n_H0"],
                    args["n_W0"],
                    args["n_C0"],
                    args["batch_size"],
                    args["training_ratio"],
                    args["embedding_size"],
                    args["image_normalization"],
                    args["same_image_epsilon"],
                    args["model_weight_path"],
                    args["model_weight_path_2"],
                    args["model_name"],
                    args["output_layer"],
                    args["num_epoch"],
                    args["MARGIN"],
                    args["net_3_dim"],
                    args["learning_rate"],
                    args["keepratio"],
                    train_size,
                    test_size,
                    args["output_table_path"])


if __name__ == '__main__':
    main()
